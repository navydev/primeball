package me.navy12333.PrimeBall;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import me.NavyDev.ArenaEvents.PickUpItem;
import me.NavyDev.ArenaEvents.PlayerChangeHelmet;
import me.NavyDev.ArenaEvents.PlayerDropItem;
import me.NavyDev.ArenaEvents.PlayerFoodChange;
import me.NavyDev.ArenaEvents.PlayerHealthChange;
import me.NavyDev.ArenaEvents.PlayerKick;
import me.NavyDev.ArenaEvents.Pre12PickUpItem;
import me.NavyDev.Ball.BallHandler;
import me.NavyDev.Ball.BallRunnable;
import me.NavyDev.Commands.CmdRoot;
import me.NavyDev.Match.MatchHandler;
import me.NavyDev.Match.MatchSecondRunnable;
import me.NavyDev.Match.MatchTickRunnable;
import me.NavyDev.MultiVersion.GetClientVersionUtil;
import me.NavyDev.MultiVersion.Version;
import me.NavyDev.PrimeBallMain.Utils.FileManager;
import me.NavyDev.Signs.OnClick;
import me.NavyDev.Signs.OnCreate;
import me.NavyDev.Signs.SignHandler;
import me.NavyDev.Signs.SignRunnable;
import me.navy12333.PrimeBallMain.listeners.Events;
import me.navy12333.PrimeBallMain.listeners.PrimeBallArena;
import net.milkbowl.vault.economy.Economy;


//Changes after the recode -
//TODO: RECODE leaderboards
//TODO Create an ability addon
//TODO Create custom events to control the plugin better
//TODO Create match object to shrink the match class.
//TODO Code an addon that allows "kits" to be added upon an arena starting.

public class PrimeBallMain extends JavaPlugin {
	public Scoreboard TBPFS;
	public Objective TBPFobj;
	public SignHandler SH = new SignHandler();
	FileManager FM;
	PrimeBallArena PBA = new PrimeBallArena();
	public BallHandler BH = new BallHandler();
	public MatchHandler MH = new MatchHandler();
    
	
	public static PrimeBallMain plugin;
	
	private static Economy econ = null;
	
	private Logger log; 
	private PluginDescriptionFile description;
	private String prefix;
	
	public static HashMap<Player, Integer> coolDown = new HashMap<Player, Integer>();
	public static ArrayList<Player> earlyLeavers = new ArrayList<Player>();
	
	File commentator;
	public FileConfiguration commentatorCfg;
	
	File agent;
	public FileConfiguration agentCfg;
	
	public Economy getEcon(){
		return econ;
	}
	
	@Override
	public void onEnable() {
	
		plugin = this;
		log = Logger.getLogger("Minecraft");
		description = getDescription();
		prefix = "[" + description.getName() + "] ";
		log("loading " + description.getFullName());
		
		Plugin p = plugin;
		FM = new FileManager();
		FM.setup(p);
		this.loadConfig();
		registerRunnables();
		registerAllEvents();
		registerCommands();
		//PBA.doAllTimeLeaderBoards(FM);
		SH.loadSigns();
		
        if (!setupEconomy() ) {
            log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
	}
	
	public void registerRunnables() {
	    getServer().getScheduler().scheduleSyncRepeatingTask(this, new BallRunnable(), 1L, 1L);
	    getServer().getScheduler().scheduleSyncRepeatingTask(this, new MatchSecondRunnable(), 0L, 20L);
	    getServer().getScheduler().scheduleSyncRepeatingTask(this, new MatchTickRunnable(), 0L, 1L);
	    getServer().getScheduler().scheduleSyncRepeatingTask(this, new CooldownRunnable(), 0L, 20L);
	    getServer().getScheduler().scheduleSyncRepeatingTask(this, new SignRunnable(), 0L, 20L);
	    return;
	}
	
	public void registerAllEvents() {
		Version serverVersion = new GetClientVersionUtil().execute();
		registerEvents(this, new Events());
		registerEvents(this, new OnClick());
		if (serverVersion.equals(Version.ONE_TWELVE)) registerEvents(this, new PickUpItem());
		else registerEvents(this, new Pre12PickUpItem());
		registerEvents(this, new PlayerDropItem());
		registerEvents(this, new PlayerFoodChange());
		registerEvents(this, new PlayerHealthChange());
		registerEvents(this, new PlayerChangeHelmet());
		registerEvents(this, new PlayerKick());
		registerEvents(this, new OnCreate());
		return;
	}
	
	
	public void registerCommands() {
		getCommand("fb").setExecutor(new CmdRoot());
		return;
	}
	public void doCoolDownTimer() {
		HashMap<Player, Integer> newTimes = new HashMap<Player, Integer>();
		if (!coolDown.isEmpty()) {
			if (!coolDown.keySet().equals(null)) {
				//Should be check for {}
				HashMap<Player,Integer> BP = coolDown;
				for (Player player: BP.keySet()) {
					int TimeLeft = Integer.parseInt(BP.get(player).toString());
					if (TimeLeft > 0){
						TimeLeft = TimeLeft - 1;
						newTimes.put(player, TimeLeft);
					} 				
				}
				coolDown.clear();
				coolDown.putAll(newTimes);
			}
		}
	}
	
    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
	public void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
		for (Listener listener : listeners) {
			Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
		}
		return;
	}
	
	@Override
	public void onDisable() {
		getServer().getScheduler().cancelAllTasks();
		SH.saveSigns();
		log("disabled " + description.getFullName());
	}

	public void log(String message) {
		log.info(prefix + message);
	}

	public void MWScoreboard(String leaderboardsPlayer) {

		Player player = Bukkit.getServer().getPlayer(UUID.fromString(leaderboardsPlayer));
		player.closeInventory();
		
		Scoreboard MG = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
		HashMap<String, Integer> leaderboardInt = new HashMap<String, Integer>();
		List<String> names = new ArrayList<String>();
		String playername = "";
		String playername2 = "";
		int playerscore = 0;
		
		
		for (int a = 1; a < 6; a++) {
			playername = FM.getMWLData().getString("Leaderboards." + "MatchesWon." + a + "." + ".Name");
			playername2 = Bukkit.getOfflinePlayer(UUID.fromString(playername)).getName();
			playerscore = FM.getMWLData().getInt("Leaderboards." + "MatchesWon." + a + "." + ".Score");
			
			leaderboardInt.put(playername2, playerscore);
			names.add(playername2);
		}
		
		
		
		Objective objective = MG.registerNewObjective("PWins", "PLoses");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "EVOBALL" + ChatColor.YELLOW + "" +
				ChatColor.BOLD + " Leaderboard");			
		
		Score Title = objective.getScore(ChatColor.AQUA + "Most Matches Won Leaderboard");
		Title.setScore(15);
		Score space1 = objective.getScore(" ");
		space1.setScore(14);
		Score Place1 = objective.getScore(ChatColor.DARK_GREEN + "1. " + names.get(0) + ": " + leaderboardInt.get(names.get(0)));
		Place1.setScore(13);
		Score Place2 = objective.getScore(ChatColor.GREEN + "2. " + names.get(1) + ": " +  leaderboardInt.get(names.get(1)));
		Place2.setScore(12);
		Score Place3 = objective.getScore(ChatColor.YELLOW + "3. " + names.get(2) + ": " + leaderboardInt.get(names.get(2)));
		Place3.setScore(11);
		Score Place4 = objective.getScore(ChatColor.GOLD + "4. " + names.get(3) + ": " + leaderboardInt.get(names.get(3)));
		Place4.setScore(10);
		Score Place5 = objective.getScore(ChatColor.RED + "5. " + names.get(4) + ": " +leaderboardInt.get(names.get(4)));
		Place5.setScore(9);
		player.getPlayer().setScoreboard(MG);

		final ScoreboardManager manager = Bukkit.getScoreboardManager();
		final Player player2 = player;
		Bukkit.getScheduler().runTaskLater(PrimeBallMain.plugin, new Runnable() {
			  @Override
			  public void run() {
				  player2.setScoreboard(manager.getNewScoreboard());
				  
			  }
			}, 200L);
	}

	public void MPScoreboard(String leaderboardsPlayer) {
		
		Player player = Bukkit.getServer().getPlayer(UUID.fromString(leaderboardsPlayer));

		player.closeInventory();
		Scoreboard MG = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
		HashMap<String, Integer> leaderboardInt = new HashMap<String, Integer>();
		List<String> names = new ArrayList<String>();
		String playername = "";
		String playername2 = "";
		int playerscore = 0;
		
		for (int a = 1; a < 6; a++) {
			playername = FM.getMPLData().getString("Leaderboards." + "MatchesPlayed." + a + "." + ".Name");
			playername2 = Bukkit.getOfflinePlayer(UUID.fromString(playername)).getName();
			playerscore = FM.getMPLData().getInt("Leaderboards." + "MatchesPlayed." + a + "." + ".Score");
			
			leaderboardInt.put(playername2, playerscore);
			names.add(playername2);
		}
		
		
		
		Objective objective = MG.registerNewObjective("PWins", "PLoses");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "EVOBALL" + ChatColor.YELLOW + "" +
				ChatColor.BOLD + " Leaderboard");			
		
		Score Title = objective.getScore(ChatColor.AQUA + "Most Matches Played Leaderboard");
		Title.setScore(15);
		Score space1 = objective.getScore(" ");
		space1.setScore(14);
		Score Place1 = objective.getScore(ChatColor.DARK_GREEN + "1. " + names.get(0) + ": " + leaderboardInt.get(names.get(0)));
		Place1.setScore(13);
		Score Place2 = objective.getScore(ChatColor.GREEN + "2. " + names.get(1) + ": " +  leaderboardInt.get(names.get(1)));
		Place2.setScore(12);
		Score Place3 = objective.getScore(ChatColor.YELLOW + "3. " + names.get(2) + ": " + leaderboardInt.get(names.get(2)));
		Place3.setScore(11);
		Score Place4 = objective.getScore(ChatColor.GOLD + "4. " + names.get(3) + ": " + leaderboardInt.get(names.get(3)));
		Place4.setScore(10);
		Score Place5 = objective.getScore(ChatColor.RED + "5. " + names.get(4) + ": " +leaderboardInt.get(names.get(4)));
		Place5.setScore(9);
		
		player.getPlayer().setScoreboard(MG);

		final ScoreboardManager manager = Bukkit.getScoreboardManager();
		final Player player2 = player;
		Bukkit.getScheduler().runTaskLater(PrimeBallMain.plugin, new Runnable() {
			  @Override
			  public void run() {
				  player2.setScoreboard(manager.getNewScoreboard());
				  
			  }
			}, 200L);
	}

	public void PVScoreboard(String leaderboardsPlayer) {

		Player player = Bukkit.getServer().getPlayer(UUID.fromString(leaderboardsPlayer));
		player.closeInventory();
		Scoreboard MG = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
		HashMap<String, Double> leaderboardDouble = new HashMap<String, Double>();
		List<String> names = new ArrayList<String>();
		String playername = "";
		String playername2 = "";
		double playerscore = 0.0;
		
		for (int a = 1; a < 6; a++) {
			playername = FM.getPVLData().getString("Leaderboards." + "PlayerValue." + a + "." + ".Name");
			playername2 = Bukkit.getOfflinePlayer(UUID.fromString(playername)).getName();
			playerscore = FM.getPVLData().getDouble("Leaderboards." + "PlayerValue." + a + "." + ".Score");
			
			leaderboardDouble.put(playername2, playerscore);
			names.add(playername2);
		}
		
		
		Objective objective = MG.registerNewObjective("PWins", "PLoses");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "EVOBALL" + ChatColor.YELLOW + "" +
				ChatColor.BOLD + " Leaderboard");			
		
		Score Title = objective.getScore(ChatColor.AQUA + "Highest Player Value Leaderboard");
		Title.setScore(15);
		Score space1 = objective.getScore(" ");
		space1.setScore(14);
		Score Place1 = objective.getScore(ChatColor.DARK_GREEN + "1. " + names.get(0) + ": " + leaderboardDouble.get(names.get(0)));
		Place1.setScore(13);
		Score Place2 = objective.getScore(ChatColor.GREEN + "2. " + names.get(1) + ": " +  leaderboardDouble.get(names.get(1)));
		Place2.setScore(12);
		Score Place3 = objective.getScore(ChatColor.YELLOW + "3. " + names.get(2) + ": " + leaderboardDouble.get(names.get(2)));
		Place3.setScore(11);
		Score Place4 = objective.getScore(ChatColor.GOLD + "4. " + names.get(3) + ": " + leaderboardDouble.get(names.get(3)));
		Place4.setScore(10);
		Score Place5 = objective.getScore(ChatColor.RED + "5. " + names.get(4) + ": " +leaderboardDouble.get(names.get(4)));
		Place5.setScore(9);
		
		player.getPlayer().setScoreboard(MG);

		final ScoreboardManager manager = Bukkit.getScoreboardManager();
		final Player player2 = player;
		Bukkit.getScheduler().runTaskLater(PrimeBallMain.plugin, new Runnable() {
			  @Override
			  public void run() {
				  player2.setScoreboard(manager.getNewScoreboard());
				  
			  }
			}, 200L);
	}
	
	public void MGScoreboard(String leaderboardsPlayer) {

		Player player = Bukkit.getServer().getPlayer(UUID.fromString(leaderboardsPlayer));
		player.closeInventory();
		Scoreboard MG = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
		HashMap<String, Integer> leaderboardInt = new HashMap<String, Integer>();
		List<String> names = new ArrayList<String>();
		String playername = "";
		String playername2 = "";
		int playerscore = 0;
		
		for (int a = 1; a < 6; a++) {
			playername = FM.getGLData().getString("Leaderboards." + "Goals." + a + "." + ".Name");
			playername2 = Bukkit.getOfflinePlayer(UUID.fromString(playername)).getName();
			playerscore = FM.getGLData().getInt("Leaderboards." + "Goals." + a + "." + ".Score");
			
			leaderboardInt.put(playername2, playerscore);
			names.add(playername2);
		}
		
		
		Objective objective = MG.registerNewObjective("PWins", "PLoses");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "EVOBALL" + ChatColor.YELLOW + "" +
				ChatColor.BOLD + " Leaderboard");			
		
		Score Title = objective.getScore(ChatColor.AQUA + "Most Goals Leaderboard");
		Title.setScore(15);
		Score space1 = objective.getScore(" ");
		space1.setScore(14);
		Score Place1 = objective.getScore(ChatColor.DARK_GREEN + "1. " + names.get(0) + ": " + leaderboardInt.get(names.get(0)));
		Place1.setScore(13);
		Score Place2 = objective.getScore(ChatColor.GREEN + "2. " + names.get(1) + ": " +  leaderboardInt.get(names.get(1)));
		Place2.setScore(12);
		Score Place3 = objective.getScore(ChatColor.YELLOW + "3. " + names.get(2) + ": " + leaderboardInt.get(names.get(2)));
		Place3.setScore(11);
		Score Place4 = objective.getScore(ChatColor.GOLD + "4. " + names.get(3) + ": " + leaderboardInt.get(names.get(3)));
		Place4.setScore(10);
		Score Place5 = objective.getScore(ChatColor.RED + "5. " + names.get(4) + ": " +leaderboardInt.get(names.get(4)));
		Place5.setScore(9);
		
		player.getPlayer().setScoreboard(MG);

		final ScoreboardManager manager = Bukkit.getScoreboardManager();
		final Player player2 = player;
		Bukkit.getScheduler().runTaskLater(PrimeBallMain.plugin, new Runnable() {
			  @Override
			  public void run() {
				 
				  player2.setScoreboard(manager.getNewScoreboard());
				  
			  }
			}, 200L);
	}

	public void loadConfig() {
        if (!getDataFolder().exists()) getDataFolder().mkdirs();
        
        File file = new File(getDataFolder(), "config.yml");
        if (!file.exists())
        	saveResource("config.yml", false);

        commentator = new File(getDataFolder(), "Commentator.yml");
        if (!commentator.exists())
        	saveResource("Commentator.yml", false);

        agent = new File(getDataFolder(), "Agent.yml");
        if (!agent.exists())
        	saveResource("Agent.yml", false);
        
        commentatorCfg = YamlConfiguration.loadConfiguration(commentator);
        agentCfg = YamlConfiguration.loadConfiguration(agent);
	}
			
  
}

 
	

