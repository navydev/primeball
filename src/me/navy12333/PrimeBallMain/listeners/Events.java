package me.navy12333.PrimeBallMain.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import me.NavyDev.Ball.BallHandler;
import me.NavyDev.Match.Match;
import me.NavyDev.MultiVersion.CheckItemInHandExistUtil;
import me.NavyDev.MultiVersion.GetClientVersionUtil;
import me.NavyDev.MultiVersion.GetPostOneNineItemInHand;
import me.NavyDev.MultiVersion.GetPreOneNineItemInHand;
import me.NavyDev.MultiVersion.Version;
import me.NavyDev.PrimeBallMain.Utils.FileManager;
import me.NavyDev.PrimeBallMain.Utils.LocationFromStringUtil;
import me.NavyDev.PrimeBallMain.Utils.RestoreInventoryUtil;
import me.navy12333.PrimeBall.PrimeBallMain;


public class Events implements Listener {
 
	FileManager FM;

	
	@EventHandler
	public void onItemDespawn(ItemDespawnEvent event) {
		BallHandler BH = PrimeBallMain.plugin.BH;
		if (!BH.getItemBalls().isEmpty()) {
			Item item = event.getEntity();
			if (BH.getItemBalls().contains(item)) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		if (PrimeBallMain.plugin.MH.getPlayerMatches().containsKey(player)) {
			event.setCancelled(true);
		}
	}
	
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		OfflinePlayer p = event.getPlayer();
		Player player = Bukkit.getPlayer(p.getUniqueId());
		if (PrimeBallMain.plugin.MH.getPlayerMatches().containsKey(player)) {
			PrimeBallMain.earlyLeavers.add(player);
			Match match = PrimeBallMain.plugin.MH.getPlayerMatches().get(player);
			match.playerLeave(player);
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		if (PrimeBallMain.earlyLeavers.contains(player)) {
			if (player.isOnline()) {
				if (new LocationFromStringUtil().execute(PrimeBallMain.plugin.getConfig().getString("hubLocation")) != null) {
					Location loc = new LocationFromStringUtil().execute(PrimeBallMain.plugin.getConfig().getString("hubLocation"));
					player.teleport(loc);
					new RestoreInventoryUtil(player);
					PrimeBallMain.earlyLeavers.remove(player);
					return;
				} else {
					Bukkit.broadcastMessage("HUB IS NOT SET");
				}
			} else {
				PrimeBallMain.earlyLeavers.add(player);
				return;
			}
		}
	    
	    if (player.getPlayer().getUniqueId().toString().equals("a33cad7e-7cd7-44e9-99ba-892f58c0c0f1")) {
	    	Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&cThe developer of the plugin &7PrimeBall &chas joined the server under the name &7" + player.getPlayer().getName()));
	    }
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInventoryClick(PlayerInteractEvent event) {
		Player player = (Player) event.getPlayer();
		if (!new CheckItemInHandExistUtil().execute(player)) return;
			Version serverVersion = new GetClientVersionUtil().execute();
			ItemStack hand;
			if (serverVersion.equals(Version.ONE_EIGHT) || serverVersion.equals(Version.ONE_NINE)) hand = new GetPreOneNineItemInHand().execute(player);
				else hand = new GetPostOneNineItemInHand().execute(player);
		ItemStack clickedItem = hand;		
		ItemMeta itemMeta = clickedItem.getItemMeta();
		
		if (itemMeta != null) {
			if (itemMeta.hasDisplayName()) {
			String itemName = itemMeta.getDisplayName();
		if (itemName.contains("Football Stats")) {
			event.setCancelled(true);
			Inventory StatsInven = Bukkit.createInventory(null, 9,ChatColor.RED + "Pick One");
			
			ItemStack stats = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
			SkullMeta  Smeta = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
			Smeta.setOwner(player.getName());	
			Smeta.setDisplayName(ChatColor.RED + "" + ChatColor.UNDERLINE + "Personal Stats");
			stats.setItemMeta(Smeta);
			
			ItemStack leaderboards = new ItemStack(Material.SIGN, 1);
			ItemMeta Lmeta = leaderboards.getItemMeta();	
			Lmeta.setDisplayName(ChatColor.RED + "" + ChatColor.UNDERLINE + "Leaderboards");
			leaderboards.setItemMeta(Lmeta);
			
			StatsInven.setItem(5, new ItemStack(stats));
			StatsInven.setItem(3, new ItemStack(leaderboards));
			player.openInventory(StatsInven);
				}
			}
		}
	}
	
    @EventHandler
    public void onPlayerClickTrinket(InventoryClickEvent event) {
    	Player player = (Player) event.getWhoClicked();
    
		ItemStack clickedItem = event.getCurrentItem();
		if (clickedItem != null) {
			ItemMeta itemMeta = clickedItem.getItemMeta();
		
		if (itemMeta != null) {
			if (itemMeta.hasDisplayName()) {
			String itemName = itemMeta.getDisplayName();
		if (event.getClickedInventory().getTitle().contains("Pick One")) {
	    	if (!player.hasPermission("fb.noinv")) {
	          if (event.getSlotType() == SlotType.QUICKBAR) { //offhand slot
	          	
	              event.setCancelled(true);
	      		}
	  		
			if (itemName.contains("Personal Stats")) {
				Bukkit.dispatchCommand(player, "fbstats");
				player.closeInventory();
				event.setCancelled(true);
			}
			if (itemName.contains("Vote")) {
				Bukkit.dispatchCommand(player, "vote");
				player.closeInventory();
				event.setCancelled(true);
			}
			else if (itemName.contains("Leaderboards")) {
				event.setCancelled(true);
				int PVPos = 0;
				int MwPos = 0;
				int MpPos = 0;
				int GsPos = 0;
				
				String player1 = player.getUniqueId().toString();
				String playerUU = "";
				List<String> list = new ArrayList<String>();
				
				
				//PV
				for(int a = 1; a < FM.getPVLData().getConfigurationSection("Leaderboards." + "PlayerValue.").getKeys(false).size(); a++) {
					list.add(FM.getPVLData().getString("Leaderboards." + "PlayerValue." + a + "." + ".Name"));
				}
				for(int a = 0; a < list.size(); a++) {
					playerUU = list.get(a);
					if (player1.contains(playerUU)) {
						PVPos = a+1;
					}
				}
				list = new ArrayList<String>();
				//GS
				for(int a = 1; a < FM.getGLData().getConfigurationSection("Leaderboards." + "Goals.").getKeys(false).size(); a++) {
					list.add(FM.getGLData().getString("Leaderboards." + "Goals." + a + "." + ".Name"));
				}
				for(int a = 0; a < list.size(); a++) {
					playerUU = list.get(a);
					if (player1.contains(playerUU)) {
						GsPos = a+1;
					}
				}
				list = new ArrayList<String>();
				//Mp
				for(int a = 1; a < FM.getMPLData().getConfigurationSection("Leaderboards." + "MatchesPlayed.").getKeys(false).size(); a++) {
					list.add(FM.getMPLData().getString("Leaderboards." + "MatchesPlayed." + a + "." + ".Name"));
				}
				for(int a = 0; a < list.size(); a++) {
					playerUU = list.get(a);
					if (player1.contains(playerUU)) {
						MpPos = a+1;
					}
				}
				list = new ArrayList<String>();
				//Mw
				for(int a = 1; a < FM.getMWLData().getConfigurationSection("Leaderboards." + "MatchesWon.").getKeys(false).size(); a++) {
					list.add(FM.getMWLData().getString("Leaderboards." + "MatchesWon." + a + "." + ".Name"));
				}
				for(int a = 0; a < list.size(); a++) {
					playerUU = list.get(a);
					if (player1.contains(playerUU)) {
						MwPos = a+1;
					}
				}
				
				player.closeInventory();
				
				Inventory LeaderboardsInven = Bukkit.createInventory(null, 9,ChatColor.RED + "Leaderboards");
				
				ItemStack MG = new ItemStack(Material.MAGMA_CREAM, 1);
				ItemMeta MGmeta = MG.getItemMeta();	
				MGmeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Most Goals");
				ArrayList<String> MGLore = new ArrayList<String>();
				MGLore.add(ChatColor.AQUA + "" + ChatColor.BOLD + "You are placed at: " + ChatColor.GOLD + "" + ChatColor.BOLD + GsPos);
				MGmeta.setLore(MGLore);
				MG.setItemMeta(MGmeta);
				
				ItemStack BPV = new ItemStack(Material.GOLD_NUGGET, 1);
				ItemMeta BPVmeta = BPV.getItemMeta();	
				
				BPVmeta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Highest Player Value");
				ArrayList<String> BPVLore = new ArrayList<String>();
				BPVLore.add(ChatColor.AQUA + "" + ChatColor.BOLD + "You are placed at: " + ChatColor.GOLD + "" + ChatColor.BOLD + PVPos);
				BPVmeta.setLore(BPVLore);
				BPV.setItemMeta(BPVmeta);
				
				ItemStack MMW = new ItemStack(Material.DIAMOND_BLOCK, 1);
				ItemMeta MMWmeta = MMW.getItemMeta();	
				MMWmeta.setDisplayName(ChatColor.AQUA + "" + ChatColor.UNDERLINE + "Most Matches Won");
				ArrayList<String> MWLore = new ArrayList<String>();
				MWLore.add(ChatColor.AQUA + "" + ChatColor.BOLD + "You are placed at: " + ChatColor.GOLD + "" + ChatColor.BOLD + MwPos);
				MMWmeta.setLore(MWLore);
				MMW.setItemMeta(MMWmeta);
				
				ItemStack MMP = new ItemStack(Material.EXP_BOTTLE, 1);
				ItemMeta MMPmeta = MMP.getItemMeta();	
				MMPmeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Most Matches Played");
				ArrayList<String> MpLore = new ArrayList<String>();
				MpLore.add(ChatColor.AQUA + "" + ChatColor.BOLD + "You are placed at: " + ChatColor.GOLD + "" + ChatColor.BOLD + MpPos);
				MMPmeta.setLore(MpLore);
				MMP.setItemMeta(MMPmeta);
				
				LeaderboardsInven.setItem(1, new ItemStack(MG));
				LeaderboardsInven.setItem(3, new ItemStack(BPV));
				LeaderboardsInven.setItem(5, new ItemStack(MMW));
				LeaderboardsInven.setItem(7, new ItemStack(MMP));
				player.closeInventory();
				player.openInventory(LeaderboardsInven);
				
			}
	    	}
		
		}
		else if (event.getClickedInventory().getTitle().contains("Leaderboards")) {
				event.setCancelled(true);
				String player1 = player.getUniqueId().toString();
				if (itemName.contains("Most Goals")) {
					PrimeBallMain.plugin.MGScoreboard(player1);
					event.setCancelled(true);
				}
				else if (itemName.contains("Highest Player Value")) {
					PrimeBallMain.plugin.PVScoreboard(player1);
					event.setCancelled(true);
				}
				else if (itemName.contains("Most Matches Won")) {
					PrimeBallMain.plugin.MWScoreboard(player1);
					event.setCancelled(true);
				}
				else if (itemName.contains("Most Matches Played")) {
					PrimeBallMain.plugin.MPScoreboard(player1);
					event.setCancelled(true);
						}
					}
				}
		}
		}
		}
	
    @EventHandler 
    public void onPlayerClickTrinket1(PlayerDropItemEvent event) {
    	Player player = event.getPlayer();
    	if (!player.hasPermission("fb.noinv")) {
    	event.setCancelled(true);
	}
    }
    

}



