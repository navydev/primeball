package me.navy12333.PrimeBallMain.listeners;


import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import me.navy12333.PrimeBall.PrimeBallMain;
import me.NavyDev.PrimeBallMain.Utils.FileManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.plugin.PluginDescriptionFile;


public class PrimeBallArena {	
	
	private Random random = new Random();
	
	public void createFireWork(Location loc, Color teamColor, int i) {
		if (loc != null && teamColor != null) {
			World w = loc.getWorld();
			for (int j = 1; j <= i; j++) {
				Entity firework = w.spawnEntity(
						new Location(w, loc.getX() + random.nextGaussian() * 3, loc.getY(), loc.getZ() + random.nextGaussian() * 3),
						EntityType.FIREWORK);
				Firework fw = (Firework) firework;
				FireworkMeta meta = fw.getFireworkMeta();

				meta.setPower(0);
				fw.setFireworkMeta(meta);
			}
		}
	}

					
	public void doAllTimeLeaderBoards(FileManager FM) {
		Bukkit.broadcastMessage(ChatColor.GRAY +  "** All time leaderboards are updating! You may experience some lag! **");
		//Top Goals
	
		int Gs = 0;		
		//Top Goals Scored
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
	 	File NewCard = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/PlayerStats/");
	 	HashMap<String, Integer> leaderboardInt = new HashMap<String, Integer>();
	 	
		for(File file : NewCard.listFiles()){
	 		 if(file.getName().endsWith(".yml")){
	 			 String playerUU = file.getName().substring(0, file.getName().length() - 4);
			
	 			 File f = new File(NewCard, File.separator + playerUU + ".yml");
	 			 FileConfiguration Team = YamlConfiguration.loadConfiguration(f);
	 			 Gs = Team.getInt("Player." + playerUU + ".GS"); 	
	 			 	leaderboardInt.put(playerUU, Gs);
	 		 }
		}
		 Set<Entry<String, Integer>> set = leaderboardInt.entrySet();
		    List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(set);
		    Collections.sort( list, new Comparator<Map.Entry<String, Integer>>()
		    {
		        public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )
		        {
		            return (o2.getValue()).compareTo( o1.getValue() );//Descending order
		        }
		    });
		    int a = 0;
		    for(Map.Entry<String, Integer> entry:list){
				a = a + 1;
				FM.getGLData().set("Leaderboards." + "Goals." + a + "." + ".Name", entry.getKey()); 
				FM.getGLData().set("Leaderboards." + "Goals." + a + "." + ".Score", entry.getValue());
		    }
		    FM.saveGLData();


		int PV = 0;
		double Pv2 = 0.0;
		//Player Value
		//One

		for(File file : NewCard.listFiles()){
	 		 if(file.getName().endsWith(".yml")){
	 			 String playerUU = file.getName().substring(0, file.getName().length() - 4);
			
	 			 File f = new File(NewCard, File.separator + playerUU + ".yml");
	 			 FileConfiguration Team = YamlConfiguration.loadConfiguration(f);
	 			 Pv2 = Team.getDouble("Player." + playerUU + ".PV");
	 			 PV = (int) (Pv2 * 100);
	 			 	leaderboardInt.put(playerUU, PV);
	 		 }
		}
		 set = leaderboardInt.entrySet();
		 list = new ArrayList<Entry<String, Integer>>(set);
		    Collections.sort( list, new Comparator<Map.Entry<String, Integer>>()
		    {
		        public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )
		        {
		            return (o2.getValue()).compareTo( o1.getValue() );//Descending order
		        }
		    });
		    a = 0;
		    for(Map.Entry<String, Integer> entry:list){
				a = a + 1;
				double PV2 = (double) entry.getValue() / 100;
				FM.getPVLData().set("Leaderboards." + "PlayerValue." + a + "." + ".Name", entry.getKey());
				FM.getPVLData().set("Leaderboards." + "PlayerValue." + a + "." + ".Score", PV2);
		    }
		    FM.savePVLData();

		
		int MP = 0;
		//Matches Played
		for(File file : NewCard.listFiles()){
	 		 if(file.getName().endsWith(".yml")){
	 			 String playerUU = file.getName().substring(0, file.getName().length() - 4);
			
	 			 File f = new File(NewCard, File.separator + playerUU + ".yml");
	 			 FileConfiguration Team = YamlConfiguration.loadConfiguration(f);
	 			 MP = Team.getInt("Player." + playerUU + ".MP");
	 			 	leaderboardInt.put(playerUU, MP);
	 		 }
		}
		 set = leaderboardInt.entrySet();
		 list = new ArrayList<Entry<String, Integer>>(set);
		    Collections.sort( list, new Comparator<Map.Entry<String, Integer>>()
		    {
		        public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )
		        {
		            return (o2.getValue()).compareTo( o1.getValue() );//Descending order
		        }
		    });
		    a = 0;
		    for(Map.Entry<String, Integer> entry:list){
				a = a + 1;
				FM.getMPLData().set("Leaderboards." + "MatchesPlayed." + a + "." + ".Name", entry.getKey()); 
				FM.getMPLData().set("Leaderboards." + "MatchesPlayed." + a + "." + ".Score", entry.getValue());
		    }
		    FM.saveMPLData();
		
		int MW = 0;
		//Matches Won
		for(File file : NewCard.listFiles()){
	 		 if(file.getName().endsWith(".yml")){
	 			 String playerUU = file.getName().substring(0, file.getName().length() - 4);
			
	 			 File f = new File(NewCard, File.separator + playerUU + ".yml");
	 			 FileConfiguration Team = YamlConfiguration.loadConfiguration(f);
	 			 MW = Team.getInt("Player." + playerUU + ".MW");

	 			 	leaderboardInt.put(playerUU, MW);
	 		 }
		}
		 set = leaderboardInt.entrySet();
		 list = new ArrayList<Entry<String, Integer>>(set);
		    Collections.sort( list, new Comparator<Map.Entry<String, Integer>>()
		    {
		        public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )
		        {
		            return (o2.getValue()).compareTo( o1.getValue() );//Descending order
		        }
		    });
		    a = 0;
		    for(Map.Entry<String, Integer> entry:list){
				a = a + 1;
				FM.getMWLData().set("Leaderboards." + "MatchesWon." + a + "." + ".Name", entry.getKey()); 
				FM.getMWLData().set("Leaderboards." + "MatchesWon." + a + "." + ".Score", entry.getValue());
		    }
		    FM.saveMWLData();
	}	
	
	
	public double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}


	
	public void doAchievements(String playerUU) {
		String playerstr = Bukkit.getPlayer(UUID.fromString(playerUU)).getName();
		

		String player1 = "";
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File NewCard = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/PlayerStats/");
		File f = new File(NewCard, File.separator + playerUU + ".yml");
		FileConfiguration Team = YamlConfiguration.loadConfiguration(f);
		String path = Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/PlayerStats/";
		Player player2 = Bukkit.getPlayer(UUID.fromString(playerUU));
		
		boolean yes = false;
		
		for(File file : NewCard.listFiles()){
	 		 if(file.getName().endsWith(".yml")){
	 			 player1 = file.getName().substring(0, file.getName().length() - 4).replaceAll(path, "");
	 			if (player1.contains(playerUU)) {
	 				yes = true;
	 			}
	 		 }
		}

		int GS = Team.getInt("Player." + playerUU + ".GS");
		int MP  = Team.getInt("Player." + playerUU + ".MP");
		//int MW  = Team.getInt("Player." + playerUU + ".MW");
		
		if (GS >= 1) {
			if (!Team.getConfigurationSection("Player." + playerUU + ".Achievements").contains("First-Goal")) {
			Bukkit.broadcastMessage(ChatColor.BLACK + "[" + ChatColor.BLUE + "EvoBall" + ChatColor.BLACK + "]"
					+ ChatColor.GOLD + playerstr + " has earned the achievement" + ChatColor.AQUA + " I'm-just-warming-up");
			
			if (yes == false) {
				try {
					f.createNewFile();
				} catch (IOException e) {
				
					e.printStackTrace();
				}
				Team.set("Player." + playerUU + ".Achievements", "First-Goal");	

			}
			else {
				Team.set("Player." + playerUU + ".Achievements", "First-Goal");	
			}
			try {
				Team.save(f);
			} catch (IOException e) {
			
				e.printStackTrace();
			}
			player2.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f[&9Agent&f] &6Well done for earning the &b'First-Goal'&6 award!"));
			}
		}
		if (GS >= 10) {
			if (!Team.getConfigurationSection("Player." + playerUU + ".Achievements").contains("Dicey-Double-Digits")) {
			Bukkit.broadcastMessage(ChatColor.BLACK + "[" + ChatColor.BLUE + "EvoBall" + ChatColor.BLACK + "]"
					+ ChatColor.GOLD + playerstr + " has earned the achievement" + ChatColor.AQUA + " Dicey-Double-Digits");
			
			if (yes == false) {
				try {
					f.createNewFile();
				} catch (IOException e) {
				
					e.printStackTrace();
				}
				Team.set("Player." + playerUU + ".Achievements", "Dicey-Double-Digits");	

			}
			else {
				Team.set("Player." + playerUU + ".Achievements", "Dicey-Double-Digits");	
			}
			try {
				Team.save(f);
			} catch (IOException e) {
			
				e.printStackTrace();
			}
			player2.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f[&9Agent&f] &6Well done for earning the &b'Dicey-Double-Digits'&6 award! You've earned this reward for scoring 10 goals!"));
			}
			}
		if (GS >= 100) {
			if (!Team.getConfigurationSection("Player." + playerUU + ".Achievements").contains("Triple-Digit-Trooper")) {
			Bukkit.broadcastMessage(ChatColor.BLACK + "[" + ChatColor.BLUE + "EvoBall" + ChatColor.BLACK + "]"
					+ ChatColor.GOLD + playerstr + " has earned the achievement" + ChatColor.AQUA + " Triple-Digit-Trooper");
			
			if (yes == false) {
				try {
					f.createNewFile();
				} catch (IOException e) {
				
					e.printStackTrace();
				}
				Team.set("Player." + playerUU + ".Achievements", "Triple-Digit-Trooper");	

			}
			else {
				Team.set("Player." + playerUU + ".Achievements", "Triple-Digit-Trooper");	
			}
			try {
				Team.save(f);
			} catch (IOException e) {
			
				e.printStackTrace();
			}
			player2.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f[&9Agent&f] &6Well done for earning the &b'Triple-Digit-Trooper'&6 award! You've earned this reward for scoring 100 goals!"));
			}
		}
		if (GS >= 1000) {
			if (!Team.getConfigurationSection("Player." + playerUU + ".Achievements").contains("Better-Than-Pele")) {
			Bukkit.broadcastMessage(ChatColor.BLACK + "[" + ChatColor.BLUE + "EvoBall" + ChatColor.BLACK + "]"
					+ ChatColor.GOLD + playerstr + " has earned the achievement" + ChatColor.RED + ChatColor.BOLD + "Better-Than-Pele!!");
			
			if (yes == false) {
				try {
					f.createNewFile();
				} catch (IOException e) {
				
					e.printStackTrace();
				}
				Team.set("Player." + playerUU + ".Achievements", "Better-Than-Pele");	

			}
			else {
				Team.set("Player." + playerUU + ".Achievements", "Better-Than-Pele");	
			}
			try {
				Team.save(f);
			} catch (IOException e) {
			
				e.printStackTrace();
			}
			player2.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f[&9Agent&f] &6Well done for earning the &b'Better-Than-Pele'&6 award! You've earned this reward for scoring 1000 goals!" ));
			}
		}
		if (MP >= 10) {
			if (!Team.getConfigurationSection("Player." + playerUU + ".Achievements").contains("Getting-To-Kicks")) {
			Bukkit.broadcastMessage(ChatColor.BLACK + "[" + ChatColor.BLUE + "EvoBall" + ChatColor.BLACK + "]"
					+ ChatColor.GOLD + playerstr + " has earned the achievement" + ChatColor.AQUA + "Getting-To-Kicks");
			
			if (yes == false) {
				try {
					f.createNewFile();
				} catch (IOException e) {
				
					e.printStackTrace();
				}
				Team.set("Player." + playerUU + ".Achievements", "Getting-To-Kicks");	

			}
			else {
				Team.set("Player." + playerUU + ".Achievements", "Getting-To-Kicks");	
			}
			try {
				Team.save(f);
			} catch (IOException e) {
			
				e.printStackTrace();
			}
			player2.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f[&9Agent&f] &6Well done for earning the &b'Getting-To-Kicks'&6 award! You've earned this award for playing 10 matches!"));
			}
		}
		if (MP >= 100) {
			if (!Team.getConfigurationSection("Player." + playerUU + ".Achievements").contains("Rematch")) {
			Bukkit.broadcastMessage(ChatColor.BLACK + "[" + ChatColor.BLUE + "EvoBall" + ChatColor.BLACK + "]"
					+ ChatColor.GOLD + playerstr + " has earned the achievement" + ChatColor.AQUA + "Rematch?");
			
			if (yes == false) {
				try {
					f.createNewFile();
				} catch (IOException e) {
				
					e.printStackTrace();
				}
				Team.set("Player." + playerUU + ".Achievements", "Rematch");	

			}
			else {
				Team.set("Player." + playerUU + ".Achievements", "Rematch");	
			}
			try {
				Team.save(f);
			} catch (IOException e) {
			
				e.printStackTrace();
			}
			player2.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f[&9Agent&f] &6Well done for earning the &b'Rematch?'&6 award! You've earned this award for playing 100 matches!"));
			}
		}
		if (MP >= 1000) {
			if (!Team.getConfigurationSection("Player." + playerUU + ".Achievements").contains("Just-One-More")) {
			Bukkit.broadcastMessage(ChatColor.BLACK + "[" + ChatColor.BLUE + "EvoBall" + ChatColor.BLACK + "]"
					+ ChatColor.GOLD + playerstr + " has earned the achievement" + ChatColor.RED + ChatColor.BOLD + "Just-One-More!!");
			
			if (yes == false) {
				try {
					f.createNewFile();
				} catch (IOException e) {
				
					e.printStackTrace();
				}
				Team.set("Player." + playerUU + ".Achievements", "Just-One-More");	

			}
			else {
				Team.set("Player." + playerUU + ".Achievements", "Just-One-More");	
			}
			try {
				Team.save(f);
			} catch (IOException e) {
			
				e.printStackTrace();
			}
			player2.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f[&9Agent&f] &6Well done for earning the &b'Just-One-More'&6 award! You've earned this award for playing 1000 matches!"));
			}
		}
	
	}










}
