package me.NavyDev.Arena;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginDescriptionFile;

import me.navy12333.PrimeBall.PrimeBallMain;

public class CheckExistsArena {

	public boolean execute(String arenaName) {
		File arenaFile = getArenaFile(arenaName);
		
		if (arenaFile.exists()) {
			FileConfiguration arenaConfig = YamlConfiguration.loadConfiguration(arenaFile);
			
			if (arenaConfig.getConfigurationSection("ArenaData.") != null) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	
	public File getArenaFile(String arenaName) {
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/");
		File f = new File(folder, File.separator + arenaName + ".yml");

		return f;
	}
}
