package me.NavyDev.Arena;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginDescriptionFile;

import me.navy12333.PrimeBall.PrimeBallMain;

public class SetPhysicsArena {

	public SetPhysicsArena(String arenaName, String physicsType) {
		File arenaFile = getArenaFile(arenaName);
		FileConfiguration arenaConfig = YamlConfiguration.loadConfiguration(arenaFile);
		
		//Spawns
		arenaConfig.set("ArenaData.Data.physicsType", physicsType);

		try {
			arenaConfig.save(arenaFile);
		} catch (IOException e) {
			Bukkit.getLogger().log(Level.WARNING, "Could not save the file for the arena " + arenaName);
			return;
		}
		return;
	}
	
	public File getArenaFile(String arenaName) {
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/");
		File f = new File(folder, File.separator + arenaName + ".yml");

		return f;
	}
}
