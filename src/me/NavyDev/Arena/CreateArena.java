package me.NavyDev.Arena;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginDescriptionFile;

import me.navy12333.PrimeBall.PrimeBallMain;

public class CreateArena {

	public CreateArena(String arenaName, String ballSpawn) {
		File arenaFile = getArenaFile(arenaName);
		if (!arenaFile.exists()) {
			try {
				arenaFile.createNewFile();
			} catch (IOException e) {
				Bukkit.getLogger().log(Level.WARNING, "Could not create the file for the arena " + arenaName);
				return;
			}
		}
		FileConfiguration arenaConfig = YamlConfiguration.loadConfiguration(arenaFile);
		
		//Spawns
		arenaConfig.set("ArenaData.Spawns.ballSpawn", ballSpawn);
		arenaConfig.set("ArenaData.Spawns.teamOneSpawn", "{}");
		arenaConfig.set("ArenaData.Spawns.teamTwoSpawn", "{}");
		
		//Data
		arenaConfig.set("ArenaData.Data.physicsType", "classic");
		arenaConfig.set("ArenaData.Data.runTime", 300);
		arenaConfig.set("ArenaData.Data.ready", false);
		
		try {
			arenaConfig.save(arenaFile);
		} catch (IOException e) {
			Bukkit.getLogger().log(Level.WARNING, "Could not save the file for the arena " + arenaName);
			return;
		}
		return;
	}
	
	public File getArenaFile(String arenaName) {
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/");
		File f = new File(folder, File.separator + arenaName + ".yml");

		return f;
	}
}
