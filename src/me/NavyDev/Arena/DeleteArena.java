package me.NavyDev.Arena;

import java.io.File;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginDescriptionFile;

import me.navy12333.PrimeBall.PrimeBallMain;

public class DeleteArena {

	public DeleteArena(String arenaName) {
		File arenaFile = getArenaFile(arenaName);
		if (!arenaFile.exists()) {
				return;
		}
		arenaFile.delete();
		return;
	}
	
	public File getArenaFile(String arenaName) {
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/");
		File f = new File(folder, File.separator + arenaName + ".yml");

		return f;
	}
}
