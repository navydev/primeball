package me.NavyDev.PrimeBallMain.Utils;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GetBoosterUtil {

	public ItemStack execute(String displayName, Material material, int amount, short type, ArrayList<String> lore) {
		ItemStack boost = new ItemStack(material, amount,(short) type);
		ItemMeta boostMeta = boost.getItemMeta();
		boostMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
		boostMeta.addEnchant(Enchantment.DAMAGE_ALL, 1, true);
		boostMeta.setLore(lore);
		boostMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		boost.setItemMeta(boostMeta);
		return boost;
	}
}
