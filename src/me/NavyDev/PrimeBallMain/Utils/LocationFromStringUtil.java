package me.NavyDev.PrimeBallMain.Utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;


public class LocationFromStringUtil {
	
    public Location execute(String stringLocation) {
    	if (stringLocation == null || stringLocation.trim() == "") {
    		return null;
    	}
    	final String[] parts = stringLocation.split(",");
    	if (parts.length == 6) {
    		World w = Bukkit.getWorld(parts[0].replaceAll("//=", "").replaceAll("\\}", ""));
    		double x = Double.parseDouble(parts[1].replaceAll("x", "").replaceAll("//=", "").replaceAll("\\}", ""));
    		double y = Double.parseDouble(parts[2].replaceAll("y", "").replaceAll("//=", "").replaceAll("\\}", ""));
    		double z = Double.parseDouble(parts[3].replaceAll("z", "").replaceAll("//=", "").replaceAll("\\}", ""));
    		float yaw = Float.parseFloat(parts[4].replaceAll("//=", "").replaceAll("\\}", ""));
    		float pitch = Float.parseFloat(parts[5].replaceAll("//=", "").replaceAll("\\}", ""));
    		return new Location(w, x, y, z, yaw, pitch);
    }
    	return null;
    }
}
