package me.NavyDev.PrimeBallMain.Utils;

import java.io.File;
import java.io.IOException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;


public class FileManager {

    Plugin p; 
    
    File PlayerStatsFile;
    
    File ArenasFile;
    
    File PlayerInventories;
    
    File Signs;
    
    FileConfiguration GoalsLeaderboardCfg;
    File GoalsLeaderboardFile;
    
    FileConfiguration MPLeaderboardCfg;
    File MPLeaderboardFile;
    
    FileConfiguration MWLeaderboardCfg;
    File MWLeaderboardFile;

    FileConfiguration PVLeaderboardCfg;
    File PVLeaderboardFile;
    
    FileConfiguration playerAcheivementCfg;
    File playerAcheivementFile;
    
    public void setup(Plugin p) {
       
    	//Files Folder
        if (!p.getDataFolder().exists()) {
                p.getDataFolder().mkdir();
        }
               
        //Declaring the file
        
        GoalsLeaderboardFile = new File(p.getDataFolder(), "GoalsLeaderboards.yml");
        MPLeaderboardFile = new File(p.getDataFolder(), "MPLeaderboards.yml");
        MWLeaderboardFile = new File(p.getDataFolder(), "MWLeaderboards.yml");
        PVLeaderboardFile = new File(p.getDataFolder(), "PVLeaderboards.yml");
        playerAcheivementFile = new File(p.getDataFolder(), "playerAchievement.yml");
        PlayerStatsFile = new File(p.getDataFolder(), "PlayerStats");
        ArenasFile = new File(p.getDataFolder(), "Arenas");
        PlayerInventories = new File(p.getDataFolder(), "Inventories");
        Signs = new File(p.getDataFolder(), "FootballSigns");
        
        if (!Signs.exists()) {
        	Signs.mkdirs();
        }
        if(!PlayerStatsFile.exists()){
            PlayerStatsFile.mkdirs();
        }
        
        if (!ArenasFile.exists()) {
        	ArenasFile.mkdirs();
        }
        
        if (!PlayerInventories.exists()) {
        	PlayerInventories.mkdirs();
        }
        
        
        if (!GoalsLeaderboardFile.exists()) {
            try {
            	GoalsLeaderboardFile.createNewFile();
            }
            catch (IOException e) {
                    Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not create GoalsLeaderboards.yml!");
            }
    }
        
        if (!MPLeaderboardFile.exists()) {
            try {
            	MPLeaderboardFile.createNewFile();
            }
            catch (IOException e) {
                    Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not create MPLeaderboards.yml!");
            }
    }
        if (!MWLeaderboardFile.exists()) {
            try {
            	MWLeaderboardFile.createNewFile();
            }
            catch (IOException e) {
                    Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not create MWLeaderboards.yml!");
            }
    }
        if (!PVLeaderboardFile.exists()) {
            try {
            	PVLeaderboardFile.createNewFile();
            }
            catch (IOException e) {
                    Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not create PVLeaderboards.yml!");
            }
    }
        
        if (!playerAcheivementFile.exists()) {
            try {
            	playerAcheivementFile.createNewFile();
            }
            catch (IOException e) {
                    Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not create playerAchievement.yml!");
            }
    }
        //File setting
        GoalsLeaderboardCfg = YamlConfiguration.loadConfiguration(GoalsLeaderboardFile);
        MPLeaderboardCfg = YamlConfiguration.loadConfiguration(MPLeaderboardFile);
        MWLeaderboardCfg = YamlConfiguration.loadConfiguration(MWLeaderboardFile);
        PVLeaderboardCfg = YamlConfiguration.loadConfiguration(PVLeaderboardFile);
        playerAcheivementCfg = YamlConfiguration.loadConfiguration(playerAcheivementFile);
    }
   


    //Goals Leaderboard file start
    
    public FileConfiguration getGLData() {
        return GoalsLeaderboardCfg;
    }

    public void saveGLData() {
    		try {
    			GoalsLeaderboardCfg.save(GoalsLeaderboardFile);
        	}
        	catch (IOException e) {
        		Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save GoalsLeaderboards.yml!");
        }
    }

    public void reloadGLData() {
    	GoalsLeaderboardCfg = YamlConfiguration.loadConfiguration(GoalsLeaderboardFile);
    }
    
    //Goals leaderboard Yaml file END
    
    
   //MP Leaderboard file start
    
    public FileConfiguration getMPLData() {
        return MPLeaderboardCfg;
    }

    public void saveMPLData() {
    		try {
    			MPLeaderboardCfg.save(MPLeaderboardFile);
        	}
        	catch (IOException e) {
        		Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save MPLeaderboards.yml!");
        }
    }

    public void reloadMPLData() {
    	MPLeaderboardCfg = YamlConfiguration.loadConfiguration(MPLeaderboardFile);
    }
    
    //MP leaderboard Yaml file END
    
    
    
    //MW Leaderboard file start
     
     public FileConfiguration getMWLData() {
         return MWLeaderboardCfg;
     }

     public void saveMWLData() {
     		try {
     			MWLeaderboardCfg.save(MWLeaderboardFile);
         	}
         	catch (IOException e) {
         		Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save MWLeaderboards.yml!");
         }
     }

     public void reloadMWLData() {
     	MWLeaderboardCfg = YamlConfiguration.loadConfiguration(MWLeaderboardFile);
     }
     
     //MW leaderboard Yaml file END
     
    //PV Leaderboard file start
     
     public FileConfiguration getPVLData() {
         return PVLeaderboardCfg;
     }

     public void savePVLData() {
     		try {
     			PVLeaderboardCfg.save(PVLeaderboardFile);
         	}
         	catch (IOException e) {
         		Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save PVLeaderboards.yml!");
         }
     }

     public void reloadPVLData() {
     	PVLeaderboardCfg = YamlConfiguration.loadConfiguration(PVLeaderboardFile);
     }
     
     //PV leaderboard Yaml file END
    
    //Player Achievements YAML file start
    public FileConfiguration getPAData() {
        return playerAcheivementCfg;
    }

    public void savePAData() {
    		try {
    			playerAcheivementCfg.save(playerAcheivementFile);
        	}
        	catch (IOException e) {
        		Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save playerAchievement.yml!");
        }
    }

    public void reloadPAData() {
    	playerAcheivementCfg = YamlConfiguration.loadConfiguration(playerAcheivementFile);
    }
    //Player Achievements YAML file end
}
