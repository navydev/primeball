package me.NavyDev.PrimeBallMain.Utils;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginDescriptionFile;

import me.navy12333.PrimeBall.PrimeBallMain;

public class RestoreInventoryUtil {

	public RestoreInventoryUtil(Player player) {
	    File file = getPlayerFile(player);
	    FileConfiguration playerConfig = YamlConfiguration.loadConfiguration(file);	
    	
	    player.getInventory().clear();
	    player.getInventory().setBoots(null);
	    player.getInventory().setChestplate(null);
	    player.getInventory().setHelmet(null);
	    player.getInventory().setLeggings(null);
	    
	    if (playerConfig.get("Inventory." + player.getUniqueId().toString() + ".contents") != null) {
	    	ArrayList<?> pcontents = (ArrayList<?>) playerConfig.get("Inventory." + player.getUniqueId().toString() + ".contents");
	    	ItemStack[] array = pcontents.toArray(new ItemStack[pcontents.size()]);
	    	player.getInventory().setContents(array);
	    }
	    if (playerConfig.get("Inventory."+ player.getUniqueId().toString() + ".armour") != null) {
	    	ArrayList<?> pcontents = (ArrayList<?>) playerConfig.get("Inventory."+ player.getUniqueId().toString() + ".armour");
	    	ItemStack[] array = pcontents.toArray(new ItemStack[pcontents.size()]);
	    	player.getInventory().setArmorContents(array);
	    }
	    

	    player.updateInventory();
		file.delete();
		return;
	}
	
	public File getPlayerFile(Player player) {
		String playerUU = player.getUniqueId().toString();
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Inventories/");
		File f = new File(folder, File.separator + playerUU + ".yml");

		return f;
	}
}
