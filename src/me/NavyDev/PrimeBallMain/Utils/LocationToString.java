package me.NavyDev.PrimeBallMain.Utils;

import org.bukkit.Location;

public class LocationToString {

	public String execute(Location loc) {
		String world = loc.getWorld().getName();
		double x = loc.getX();
		double y = loc.getY();
		double z = loc.getZ();
		double yaw = loc.getYaw();
		double pitch = loc.getPitch();
		
		return world+","+x+","+y+","+z+","+yaw+","+pitch;
	}
}
