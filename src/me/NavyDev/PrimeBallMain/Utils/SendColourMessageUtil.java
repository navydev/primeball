package me.NavyDev.PrimeBallMain.Utils;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class SendColourMessageUtil {

	public SendColourMessageUtil(Player player, String message) {
		message = ChatColor.translateAlternateColorCodes('&', message);
		player.sendMessage(message);
	}
}
