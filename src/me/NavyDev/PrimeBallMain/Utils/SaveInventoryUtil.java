package me.NavyDev.PrimeBallMain.Utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginDescriptionFile;

import me.navy12333.PrimeBall.PrimeBallMain;

public class SaveInventoryUtil {

	public SaveInventoryUtil(Player player) {
		ItemStack[] pcontents = player.getInventory().getContents();
	    ItemStack[] parmour = player.getInventory().getArmorContents();  
	    File file = getPlayerFile(player);
	    FileConfiguration playerConfig = YamlConfiguration.loadConfiguration(file);	
	    playerConfig.set("Inventory."+ player.getUniqueId().toString() + ".contents", pcontents);
	    playerConfig.set("Inventory." + player.getUniqueId().toString() + ".armour", parmour);
	    try {
			playerConfig.save(file);
		} catch (IOException e) {
			Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could save " + player.getName() + " inventory contents.");
		}
	    return;
	}
	
	public File getPlayerFile(Player player) {
		String playerUU = player.getUniqueId().toString();
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Inventories/");
		File f = new File(folder, File.separator + playerUU + ".yml");

		return f;
	}
}
