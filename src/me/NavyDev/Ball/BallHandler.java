package me.NavyDev.Ball;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

import org.bukkit.entity.Item;

public class BallHandler {
	

	HashSet<Ball> balls = new HashSet<Ball>();
	HashSet<Item> ballItems = new HashSet<Item>();
	private HashSet<UUID> ballIDs = new HashSet<UUID>();
	private HashMap<UUID, Ball> itemBall = new HashMap<UUID, Ball>();


	public void tick() {
		if (balls != null) {
			if (!balls.isEmpty()) {
				ArrayList<Ball> ballList = new ArrayList<Ball>();
				ballList.addAll(balls);
				for (Ball ball : ballList) {
					if (!ball.item.isDead()) {
						ball.doPhysics();
					} else {
						balls.remove(ball);
						ballItems.remove(ball.item);
					}
				}
			}
		}
	}
	
	public void addBallIds(UUID id) {
		this.ballIDs.add(id);
	}
	
	public void removeBallIds(UUID id) {
		this.ballIDs.remove(id);
	}

	public void despawnBall(UUID id) {
		Ball ball = itemBall.get(id);
		Item item = ball.item;
		balls.remove(ball);
		ballItems.remove(item);
		item.remove();
	}
	public HashSet<Item> getItemBalls() {
		return this.ballItems;
	}
	
	public void removeBallItem(Item item) {
		this.ballItems.remove(item);
	}
	
	public void addBallItem(Item item) {
		this.ballItems.add(item);
	}
	public HashSet<Ball> getBalls() {
		return balls;
	}
	
	public void removeBall(Ball ball) {
		balls.remove(ball);
	}
	public void addBall(Ball ball) {
		balls.add(ball);
	}

	public HashSet<UUID> getBallIDs() {
		return ballIDs;
	}

	public void setBallIDs(HashSet<UUID> ballIDs) {
		this.ballIDs = ballIDs;
	}

	public void putBallID(UUID id, Ball ball) {
		this.itemBall.put(id, ball);
	}
	
	public void takeBallID(UUID id) {
		this.itemBall.remove(id);
	}
	public HashMap<UUID, Ball> getItemBall() {
		return itemBall;
	}

	public void setItemBall(HashMap<UUID, Ball> itemBall) {
		this.itemBall = itemBall;
	}
}
