package me.NavyDev.Ball;

import java.util.UUID;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import me.NavyDev.Match.Match;
import me.NavyDev.Match.MatchHandler;
import me.NavyDev.MultiVersion.GetClientVersionUtil;
import me.NavyDev.MultiVersion.Version;
import me.navy12333.PrimeBall.PrimeBallMain;

public class Ball {

	protected Item item;
	protected UUID id;
	protected ItemStack BallItemStack = PrimeBallMain.plugin.getConfig().getItemStack("ball");
	private Vector vectors;
	private Location lastKickedFrom;
	private Player lastKickedBy;
	private boolean particles = true;
	private String particle = PrimeBallMain.plugin.getConfig().getString("ParticlesType");
	public Ball() {
		super();
	}
	
	public enum physicsType {
		classic,
		navySpecial;
	}
	
	public void doPhysics() {
		if (!this.getItem().isDead()) {
			Vector velocity = ((Entity) this.item).getVelocity();
			if (this.vectors != null) {
				velocity = this.vectors;
			}
			Vector newVector = ((Entity) this.item).getVelocity();
			if (newVector.getX() == 0.0D) {
				newVector.setX(-velocity.getX() * 0.9D);
			} else if (Math.abs(velocity.getX() - newVector.getX()) < 0.15D) {
				newVector.setX(velocity.getX() * 0.975D);
			}
			if ((newVector.getY() == 0.0D) && (velocity.getY() < -0.1D)) {
				newVector.setY(-velocity.getY() * 0.9D);
			}
			if (newVector.getZ() == 0.0D) {
				newVector.setZ(-velocity.getZ() * 0.9D);
			} else if (Math.abs(velocity.getZ() - newVector.getZ()) < 0.15D) {
				newVector.setZ(velocity.getZ() * 0.975D);
			}
			this.vectors = newVector;
			((Entity) this.item).setVelocity(newVector);

			if (this.particles) {
				showEffect(item);
			}
		}
	}
	
	public void doKick(Player player) {
		Location location = player.getLocation();
		World world = player.getWorld();
		Vector kV = kickVector(player);
		item.setVelocity(kV);
		this.vectors = kV;
		this.lastKickedFrom = player.getLocation();
					
		world.playEffect(location, Effect.STEP_SOUND, 10);
		this.lastKickedBy = player;
	}
	
	public Ball spawnBall(Location loc) {
		World world = loc.getWorld();
		if (!BallItemStack.getType().equals(Material.AIR)) {
			this.item = world.dropItem(loc, BallItemStack);	
		} else {
			this.item = world.dropItem(loc, new ItemStack(Material.MAGMA_CREAM));	
		}
		this.item.setVelocity(new Vector(0, 0, 0));
		this.id = this.item.getUniqueId();
		PrimeBallMain.plugin.BH.balls.add(this);
		PrimeBallMain.plugin.BH.addBall(this);
		PrimeBallMain.plugin.BH.addBallItem(this.item);
		PrimeBallMain.plugin.BH.addBallIds(this.id);
		PrimeBallMain.plugin.BH.putBallID(id, this);
		return this;
	}
	

	@SuppressWarnings("deprecation")
	public void showEffect(Entity entity) {
		Location location = entity.getLocation();
		World world = entity.getWorld();
		
		Version serverVersion = new GetClientVersionUtil().execute();
		
		if (serverVersion.equals(Version.ONE_EIGHT)) {
			if (!entity.isDead()) {
				Effect effect = getEightEffect();
				if (effect != null) {
					world.playEffect(location, effect, 1);
				} else {
					world.playEffect(location, Effect.INSTANT_SPELL, 1);
				}
			}
		} else {
			if (!entity.isDead()) {
				Particle effect = getPostEightParticle();
				if (effect != null) {
					world.spawnParticle(effect, location, 1);
				} else {
					world.spawnParticle(Particle.SPELL_INSTANT, location, 1);
				} 
			}
		}
	}	
	
	
	@SuppressWarnings("deprecation")
	public Effect getEightEffect() {
		for (Effect effect : Effect.values()) {
			if (effect.toString().equals(particle)) return effect;
		}
		Effect effect = Effect.INSTANT_SPELL;
		return effect;
	}
	
	public Particle getPostEightParticle() {
		for (Particle effect : Particle.values()) {
			if (effect.toString().equals(particle)) return effect;
		}
		return Particle.SPELL_INSTANT;
	}
	public Vector kickVector(Player player) {
		double configPower = PrimeBallMain.plugin.getConfig().getDouble("power");
		float adjPitch = -15.0f;
		float maxPitch = -90.0f;
		
		Location loc = player.getEyeLocation();

		float pitch = loc.getPitch();
		pitch += adjPitch;
		
		if (playerInArena(player)) {
			if (getPhysics(player).equals(physicsType.classic)) {
				pitch = doClassicPhysics(pitch, maxPitch);
			} else {
				pitch = doNavyPhysics(pitch);
			}
		} else {
			pitch = doClassicPhysics(pitch, maxPitch);
		}

		loc.setPitch(pitch);
		Vector vector = loc.getDirection();
		vector = vector.multiply(configPower);
		return vector;
	}
	
	public float doClassicPhysics(float ptch, float maxPitch) {
		float pitch = ptch;
		if (pitch > 0.0F) {
			pitch = 0.0F;
		}
		if (pitch < maxPitch) {
			pitch = 0.0F + maxPitch;
		}
		return pitch;
	}
	
	public float doNavyPhysics(float ptch) {
		float pitch = ptch;
		if (pitch < - 79.0F) {
			pitch = - 89.0F;
		}
		return pitch;
	}
	
	public physicsType getPhysics(Player player) {
		MatchHandler MH = PrimeBallMain.plugin.MH;
		Match match = MH.getPlayerMatches().get(player);
		return match.getPhysicsType();
	}
	public boolean playerInArena(Player player) {
		MatchHandler MH = PrimeBallMain.plugin.MH;
		if (!MH.getPlayers().isEmpty()) {
			if (MH.getPlayers().contains(player)) {
				return true;
			}
		} 
		return false;
	}
	
	public Item getItem() {
		return this.item;
	}
	
	public ItemStack getItemStack() {
		return this.BallItemStack;
	}
	
	public void setItemStack(ItemStack item) {
		this.BallItemStack = item;
	}

	public String getParticle() {
		return particle;
	}

	public void setParticle(String particle) {
		this.particle = particle;
	}

	public Player getLastKickedBy() {
		return lastKickedBy;
	}

	public void setLastKickedBy(Player lastKickedBy) {
		this.lastKickedBy = lastKickedBy;
	}

	public Location getLastKickedFrom() {
		return lastKickedFrom;
	}

	public void setLastKickedFrom(Location lastKickedFrom) {
		this.lastKickedFrom = lastKickedFrom;
	}
}
