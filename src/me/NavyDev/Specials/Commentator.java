package me.NavyDev.Specials;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import me.NavyDev.Match.Match;
import me.navy12333.PrimeBall.PrimeBallMain;
import net.md_5.bungee.api.ChatColor;

public class Commentator {

	private Random rand = new Random();
	private String prefix = "&0[&cCommentator&0] ";
	private int maxSG;
	private int maxMG;

	public String execute(Player player, int distance, Match match) {
		String playerUUID = player.getUniqueId().toString();
		File playerFile = getPlayerFile(player);
		FileConfiguration playerConfig = YamlConfiguration.loadConfiguration(playerFile);
		int GS = playerConfig.getInt("Player." + playerUUID + ".GS");

		if (!casualArena(match)) {
			if (GS == 1) {
				String str = prefix + PrimeBallMain.plugin.commentatorCfg.getString("FirstGoal");
				str = ChatColor.translateAlternateColorCodes('&', str);
				str = str.replace("%player_name%", player.getName());
				str = str.replace("%yards%", "" + distance);
				return str;
			}
		}
		int devrand = rand.nextInt(60);
		if (player.getUniqueId().toString().equals("a33cad7e-7cd7-44e9-99ba-892f58c0c0f1") && devrand == 10) {
			return ChatColor.translateAlternateColorCodes('&',
					prefix + "&7God. Only the developer of PrimeBall could score a goal like that.");
		}
		if (PrimeBallMain.plugin.getConfig().contains(".maxShortGoal")) {
			maxSG = (PrimeBallMain.plugin.getConfig().getInt(".maxShortGoal"));
		} else {
			maxSG = 5;
		}

		if (PrimeBallMain.plugin.getConfig().contains(".maxMediumGoal")) {
			maxMG = (PrimeBallMain.plugin.getConfig().getInt(".maxMediumGoal"));
		} else {
			maxMG = 20;
		}

		if (distance <= maxSG) {
			String id = player.getUniqueId().toString();
			if (playerConfig.getConfigurationSection("Player.") != null) {
				int SG = playerConfig.getInt("Player." + id + ".SG");
				playerConfig.set("Player." + id + ".SG", SG + 1 );
			} 
			
			try {
				playerConfig.save(playerFile);
			} catch (IOException e) {

			}
		
			ArrayList<String> shortgoal = new ArrayList<String>();
			if (PrimeBallMain.plugin.commentatorCfg.getStringList("ShortGoal").isEmpty())
				return "";
			for (String str : PrimeBallMain.plugin.commentatorCfg.getStringList("ShortGoal")) {
				str = ChatColor.translateAlternateColorCodes('&', prefix + str);
				str = str.replace("%player_name%", player.getName());
				str = str.replace("%yards%", "" + distance);
				shortgoal.add(str);
			}
			int replyNumber = rand.nextInt(PrimeBallMain.plugin.commentatorCfg.getStringList("ShortGoal").size());
			return shortgoal.get(replyNumber);
		}

		else {

			if (distance <= maxMG) {
				String id = player.getUniqueId().toString();
				if (playerConfig.getConfigurationSection("Player.") != null) {
					int NG = playerConfig.getInt("Player." + id + ".NG");
					playerConfig.set("Player." + id + ".NG", NG + 1 );
				} 
				
				try {
					playerConfig.save(playerFile);
				} catch (IOException e) {

				}
				ArrayList<String> mediumgoal = new ArrayList<String>();
				if (PrimeBallMain.plugin.commentatorCfg.getStringList("MediumGoal").isEmpty())
					return "";
				for (String str : PrimeBallMain.plugin.commentatorCfg.getStringList("MediumGoal")) {
					str = ChatColor.translateAlternateColorCodes('&', prefix + str);
					str = str.replace("%player_name%", player.getName());
					str = str.replace("%yards%", "" + distance);
					mediumgoal.add(str);
				}
				int replyNumber2 = rand.nextInt(PrimeBallMain.plugin.commentatorCfg.getStringList("MediumGoal").size());
				return mediumgoal.get(replyNumber2);
			}

			else {
				
				String id = player.getUniqueId().toString();
				if (playerConfig.getConfigurationSection("Player.") != null) {
					int LG = playerConfig.getInt("Player." + id + ".LG");
					playerConfig.set("Player." + id + ".LG", LG + 1 );
				} 
				
				try {
					playerConfig.save(playerFile);
				} catch (IOException e) {

				}

				ArrayList<String> longgoal = new ArrayList<String>();
				if (PrimeBallMain.plugin.commentatorCfg.getStringList("LongGoal").isEmpty())
					return "";
				for (String str : PrimeBallMain.plugin.commentatorCfg.getStringList("LongGoal")) {
					str = ChatColor.translateAlternateColorCodes('&', prefix + str);
					str = str.replace("%player_name%", player.getName());
					str = str.replace("%yards%", "" + distance);
					longgoal.add(str);
				}
				int replyNumber1 = rand.nextInt(PrimeBallMain.plugin.commentatorCfg.getStringList("LongGoal").size());
				return longgoal.get(replyNumber1);
			}
		}
	}

	public File getPlayerFile(Player player) {
		String playerUU = player.getUniqueId().toString();
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(
				Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/PlayerStats/");
		File f = new File(folder, File.separator + playerUU + ".yml");

		return f;
	}

	public boolean casualArena(Match match) {
		String arenaName = match.getMatchName();
		if (PrimeBallMain.plugin.getConfig().getStringList("casualArenas") != null) {
			List<String> casualArenas = PrimeBallMain.plugin.getConfig().getStringList("casualArenas");
			if (casualArenas.contains(arenaName)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

}
