package me.NavyDev.Specials;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import me.navy12333.PrimeBall.PrimeBallMain;

public class Agent {

	private Random rand = new Random();
	private String prefix = "&f[&bAgent&f] ";
	
	public Agent(Player player) {
		String playerUUID = player.getUniqueId().toString();
		File playerFile = getPlayerFile(player);
		FileConfiguration playerConfig = YamlConfiguration.loadConfiguration(playerFile);
		int GS = playerConfig.getInt("Player." + playerUUID + ".GS");
		
		if (GS == 1) {
			player.sendMessage(" ");
			String str = prefix + PrimeBallMain.plugin.agentCfg.getString("FirstGoal");
			str = ChatColor.translateAlternateColorCodes('&', str);
			str = str.replace("%player_name%", player.getName());
			player.sendMessage(str);
			player.sendMessage(" ");
			return;
		}
		
		ArrayList<String> messages = new ArrayList<String>();
		if (PrimeBallMain.plugin.agentCfg.getStringList("Messages").isEmpty()) return;
		for (String str : PrimeBallMain.plugin.agentCfg.getStringList("Messages")) {
			str = ChatColor.translateAlternateColorCodes('&', prefix + str);
			str = str.replace("%player_name%", player.getName());
			str = str.replace("%GS%", "" + GS);
			messages.add(str);
		}
		int replyNumber = rand.nextInt(messages.size() - 1);
		player.sendMessage(messages.get(replyNumber));

	}
	
	public File getPlayerFile(Player player) {
		String playerUU = player.getUniqueId().toString();
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/PlayerStats/");
		File f = new File(folder, File.separator + playerUU + ".yml");

		return f;
	}
}