package me.NavyDev.Signs;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.scheduler.BukkitScheduler;

import me.NavyDev.Match.Match;
import me.NavyDev.Match.Match.stages;
import me.NavyDev.Match.MatchHandler;
import me.NavyDev.PrimeBallMain.Utils.LocationFromStringUtil;
import me.NavyDev.PrimeBallMain.Utils.LocationToString;
import me.navy12333.PrimeBall.PrimeBallMain;

public class SignHandler {

	ArrayList<Sign> Signs = new ArrayList<Sign>();
	
	public void tickSign() {
		if (Signs != null) {
			if (!Signs.isEmpty()) {
				ArrayList<Sign> signs = new ArrayList<Sign>();
				signs.addAll(Signs);
				for (Sign sign : signs) {
					if (sign.getLocation().getBlock().getType().equals(Material.WALL_SIGN) || sign.getLocation().getBlock().getType().equals(Material.SIGN_POST)) {
						if (sign.getLine(2) != null) {
							if (!sign.getLine(2).equals("")) {
								Bukkit.broadcastMessage("1: " + sign.getLine(0) + " 2: " + sign.getLine(1) + " 3: " + sign.getLine(2) + " 4: " + sign.getLine(3));
								updateSign(sign, sign.getLine(2));
							}
						}
					} else {
						Signs.remove(sign);
					}
				}
			}
		}
	}
	
	public void updateSign(final Sign sign, String arenaName) {
		MatchHandler MH = PrimeBallMain.plugin.MH;
		if (MH.getMatches() != null) {
			if (MH.getStringMatches().containsKey(arenaName)) {
				Match match = MH.getStringMatches().get(arenaName);
				if (match.getStage().equals(stages.ACTIVE)) {
					int timeLeft = match.getTimeLeft();
					String lineFour = ChatColor.translateAlternateColorCodes('&', "&3&nActive " + "&7(" + timeLeft + ")");
					sign.setLine(3, lineFour);
				} else {
					int timeLeft = match.getTimeLeft();
					String lineFour = ChatColor.translateAlternateColorCodes('&', "&a&nSTARTING " + "&7(" + timeLeft + ")");
					sign.setLine(3, lineFour);
				}
			} else {
				String lineFour = ChatColor.translateAlternateColorCodes('&', "&l&8Waiting");
				sign.setLine(3, lineFour);
			}
		}
		BukkitScheduler scheduler = PrimeBallMain.plugin.getServer().getScheduler();
	    scheduler.scheduleSyncDelayedTask(PrimeBallMain.plugin, new Runnable() {
	        @Override
	      public void run() {
	        	sign.update(true);
	        }
	    }, 2L);
	}
	
	public void loadSigns() {
		if (getSigns() != null) {
			File[] files = getSigns();
			for (File file : files) {
				FileConfiguration signConfig = YamlConfiguration.loadConfiguration(file);
				if (signConfig.getConfigurationSection("sign.") != null) {
					Location loc = new LocationFromStringUtil().execute(signConfig.getString("sign.loc"));
					Block block = loc.getBlock();
					if (block instanceof Sign) {
						Sign sign = (Sign) block;
						Signs.add(sign);
					}
				}
			}
		}
	}
	
	public void saveSigns() {
		if (Signs != null) {
			if (!Signs.isEmpty()) {
				PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
				File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/FootballSigns/");
				folder.delete();
				folder.mkdirs();
				ArrayList<Sign> signs = new ArrayList<Sign>();
				signs.addAll(Signs);
				for (Sign sign : signs) {
					Location loc = sign.getLocation();
					String signLoc = loc.getWorld().getName() + round(loc.getX(), 2) + round(loc.getY(), 2) + round(loc.getZ(), 2);
					File file = new File(folder, File.separator +  signLoc + ".yml");	
					FileConfiguration signConfig = YamlConfiguration.loadConfiguration(file);
					String locationString = new LocationToString().execute(loc);
					signConfig.set("sign.loc", locationString);
					try {
						signConfig.save(file);
					} catch (IOException e) {
						
					}
				}
			}
		}
	}
	
	
	public File[] getSigns() {
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/FootballSigns/");
		File[] f = folder.listFiles();

		return f;
	}
	
	public double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}
