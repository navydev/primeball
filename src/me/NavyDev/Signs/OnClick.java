package me.NavyDev.Signs;

import java.io.File;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.PluginDescriptionFile;

import me.NavyDev.Match.Match;
import me.NavyDev.Match.MatchHandler;
import me.NavyDev.Match.Match.stages;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;
import me.navy12333.PrimeBall.PrimeBallMain;

public class OnClick implements Listener {
	
	//TODO add sign joining with teams.
	
	@EventHandler
	public void onClick(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (event.getClickedBlock() != null) {
			Block clickedBlock = event.getClickedBlock();
			if (clickedBlock.getState() != null) {
				if (clickedBlock.getState().getType() != null) {
					if (clickedBlock.getState().getType().equals(Material.WALL_SIGN) || clickedBlock.getState().getType().equals(Material.SIGN_POST)) {
						Sign clickedSign = (Sign) clickedBlock.getState();
						if (clickedSign.getLines() != null) {
							String lineOne = ChatColor.stripColor(clickedSign.getLine(0));
							if (lineOne.equals("[Futbol]")) {
								String lineTwo = ChatColor.stripColor(clickedSign.getLine(1));
								if (lineTwo.equalsIgnoreCase("join")) {
									if (!clickedSign.getLine(2).equals("")) { 
										String arenaName = ChatColor.stripColor(clickedSign.getLine(2));
										doJoinArena(player, arenaName.toUpperCase());
									} else {
										doJoin(player);
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	public void doJoinArena(Player player, String arenaName) {
		MatchHandler MH = PrimeBallMain.plugin.MH;
		arenaName = arenaName.toUpperCase();
		if (!MH.getPlayerMatches().containsKey(player)) {
			if (MH.getStringMatches().containsKey(arenaName)) {
				Match match = MH.getStringMatches().get(arenaName);
				if (!match.getStage().equals(stages.WAITING)) {
					match.playerLateJoin(player, null);
					new SendColourMessageUtil(player, "&aJoined the arena " + arenaName);
				} else {
					match.playerJoin(player, null);
					new SendColourMessageUtil(player, "&aJoined the arena " + arenaName);
				}
			} else {
				PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
				File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/");
				File f = new File(folder, File.separator + arenaName + ".yml");
				if (arenaReady(f)) {
					Match match = new Match(arenaName, MH);
					match.playerJoin(player, null);
					new SendColourMessageUtil(player, "&aJoined the arena " + arenaName);
				} else {
					new SendColourMessageUtil(player, "&aThe arena " + arenaName + " is not ready to be played in.");
				}
			}
		}
	}
	
	public void doJoin(Player player) {
		MatchHandler MH = PrimeBallMain.plugin.MH;
		if (MH.getMatches() != null) {
			if (!MH.getMatches().isEmpty()) {
				Random rand = new Random();
				int no = rand.nextInt(MH.getMatches().size());
				Match match = MH.getMatches().get(no);
				match.playerLateJoin(player, null);
				new SendColourMessageUtil(player, "&aJoined the arena " + match.getMatchName());
			} else {
				if (getArenaFile() != null) {
					File file = getArenaFile();
					if (arenaReady(file)) {
						PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
						String path = Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/";
						String arenaName = file.getName().substring(0, file.getName().length() - 4).replaceAll(path, "");
						Match match = new Match(arenaName, MH);
						match.playerJoin(player, null);
						new SendColourMessageUtil(player, "&aJoined the arena " + match.getMatchName());
					} else {
						new SendColourMessageUtil(player, "&cSorry, we could not find an arena for you. Try again in a while.");
					}
				} else {
					new SendColourMessageUtil(player, "&cSorry, there are currently no arenas to play in.");
				}
			}
		}
	}
		
	public File getArenaFile() {
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/");
		
		File[] files = folder.listFiles();
		Random rand = new Random();
		int no = rand.nextInt(files.length);
		
		File f = files[no];
		return f;
	}
		
	public boolean arenaReady(File file) {
		FileConfiguration arenaConfig = YamlConfiguration.loadConfiguration(file);
		
		if (arenaConfig.getBoolean("ArenaData.Data.ready")) {
			return true;
		} else {
			if (arenaConfig.getString("ArenaData.Spawns.ballSpawn").equals("{}")) {
				return false;
			} else if (arenaConfig.getString("ArenaData.Spawns.teamOneSpawn").equals("{}")) {
				return false;
			} else if (arenaConfig.getString("ArenaData.Spawns.teamTwoSpawn").equals("{}")) {
				return false;
			} else {
				arenaConfig.set("ArenaData.Data.ready", true);
				return true;
			}
		}
	}
			

}
		
