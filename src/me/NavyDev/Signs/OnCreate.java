package me.NavyDev.Signs;

import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

import me.navy12333.PrimeBall.PrimeBallMain;

public class OnCreate implements Listener {

	@EventHandler
	public void onCreate(SignChangeEvent event) {
		if (event.getLines() != null) {
			if (event.getLine(0) != null) {
				String lineOne = ChatColor.stripColor(event.getLine(0));
				if (lineOne.equals("[Futbol]")) {
					lineOne = ChatColor.translateAlternateColorCodes('&', "&0[&8Futbol&0]");
					event.setLine(0, lineOne);
					if (event.getLine(1) != null) {
						String lineTwo = ChatColor.stripColor(event.getLine(1));
						if (lineTwo.equalsIgnoreCase("join")) {
							lineTwo = ChatColor.translateAlternateColorCodes('&', "&aJoin");
							event.setLine(1, lineTwo);
						}	
						if (event.getLine(2) != null)  {
							String lineThree = ChatColor.stripColor(event.getLine(2));	
							lineThree = ChatColor.translateAlternateColorCodes('&', "&7" + lineThree);
							event.setLine(2, lineThree);	
						}
							
						}
					final Sign sign = (Sign) event.getBlock().getState();
					PrimeBallMain.plugin.SH.Signs.add(sign);
					
				}
			}	
		}
	}
}
