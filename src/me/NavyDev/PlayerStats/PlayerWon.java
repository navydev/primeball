package me.NavyDev.PlayerStats;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import me.navy12333.PrimeBall.PrimeBallMain;

public class PlayerWon {

	public PlayerWon(Player player) {
			
		File playerFile = getPlayerFile(player);
		FileConfiguration playerConfig = YamlConfiguration.loadConfiguration(playerFile);
		String playerUU = player.getUniqueId().toString();
		
		if (playerConfig.getConfigurationSection("Player.") == null) {
			playerConfig.set("Player." + playerUU + ".MP", 1);
			playerConfig.set("Player." + playerUU + ".MW", 1);
			playerConfig.set("Player." + playerUU + ".ML", 0);
			playerConfig.set("Player." + playerUU + ".MD", 0);
			playerConfig.set("Player." + playerUU + ".GS", 0);
			playerConfig.set("Player." + playerUU + ".AS", 0);
			playerConfig.set("Player." + playerUU + ".OG", 0);
			playerConfig.set("Player." + playerUU + ".PN", 0);
			playerConfig.set("Player." + playerUU + ".PV", 0);	
			playerConfig.set("Player." + playerUU + ".PO", 3);
		} else {	
			int MP = playerConfig.getInt("Player." + playerUU + ".MP");
			int MW = playerConfig.getInt("Player." + playerUU + ".MW");
			int PO = playerConfig.getInt("Player." + playerUU + ".PO");
			playerConfig.set("Player." + playerUU + ".MP", MP + 1);
			playerConfig.set("Player." + playerUU + ".MW", MW + 1);
			playerConfig.set("Player." + playerUU + ".PO", PO + 3);
		}
		try {
			playerConfig.save(playerFile);
		} catch (IOException e) {
			Bukkit.getLogger().log(Level.WARNING, "Could not save " + player.getName() + "'s stats. [PrimeBall]");
		}
		int MP = playerConfig.getInt("Player." + playerUU + ".MP");
		int MW = playerConfig.getInt("Player." + playerUU + ".MW");
		if (player.isOnline()){
			player.sendMessage(" ");
			player.sendMessage(ChatColor.WHITE + "[" + ChatColor.BLUE + "Agent" + ChatColor.WHITE + "]"
					+ ChatColor.GOLD + " Ah! Amazing work on that win! You'll be happy to know you've now won "
					+ MW + " out of the " + MP + " games you've played!");
		}
		return;
	}
	
	public File getPlayerFile(Player player) {
		String playerUU = player.getUniqueId().toString();
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/PlayerStats/");
		File f = new File(folder, File.separator + playerUU + ".yml");
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return f;
	}
}
