package me.NavyDev.PlayerStats;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import me.navy12333.PrimeBall.PrimeBallMain;

public class PlayerGoalScored {

	public PlayerGoalScored(Player player) {
		File playerFile = getPlayerFile(player);
		FileConfiguration playerConfig = YamlConfiguration.loadConfiguration(playerFile);
		String id = player.getUniqueId().toString();
		if (playerConfig.getConfigurationSection("Player.") != null) {
			int GS = playerConfig.getInt("Player." + id + ".GS");
			playerConfig.set("Player." + id + ".GS", GS + 1 );
		} else {
			playerConfig.set("Player." + id + ".MP", 0);
			playerConfig.set("Player." + id + ".MW", 0);
			playerConfig.set("Player." + id + ".ML", 0);
			playerConfig.set("Player." + id + ".MD", 0);
			playerConfig.set("Player." + id + ".GS", 1);
			playerConfig.set("Player." + id + ".AS", 0);
			playerConfig.set("Player." + id + ".OG", 0);
			playerConfig.set("Player." + id + ".PN", 0);
			playerConfig.set("Player." + id + ".PO", 0);
		}
		
		try {
			playerConfig.save(playerFile);
		} catch (IOException e) {

		}
	}
	
	
	public File getPlayerFile(Player player) {
		String playerUU = player.getUniqueId().toString();
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/PlayerStats/");
		File f = new File(folder, File.separator + playerUU + ".yml");
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return f;
	}
	
}
