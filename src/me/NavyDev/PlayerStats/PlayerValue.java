package me.NavyDev.PlayerStats;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import me.navy12333.PrimeBall.PrimeBallMain;

public class PlayerValue {

	public PlayerValue(Player player) {
		
		String playerUU = player.getUniqueId().toString();		

		File playerFile = getPlayerFile(player);
		FileConfiguration playerConfig = YamlConfiguration.loadConfiguration(playerFile);
		
		double PV = 0.0;
		if (playerConfig.getConfigurationSection("Player.") != null) {
			PV = playerConfig.getDouble("Player." + playerUU + ".PV");
		}
		
		int MP = playerConfig.getInt("Player." + playerUU + ".MP");
		int MW = playerConfig.getInt("Player." + playerUU + ".MW");
		int GS = playerConfig.getInt("Player." + playerUU + ".GS");
		int OG = playerConfig.getInt("Player." + playerUU + ".OG");
		
		double MWd = MW;
		double MPd = MP;
		double gsog = (double) (((GS+1) * 0.1) / ((OG+1) * 0.1));
		double mpgsr = (double) ((MP * gsog) * 0.1);
		double perc = (MWd/MPd);
		
		PV = (double) (mpgsr * perc) / 100;
		
		PV = round(PV, 2);	
		
		playerConfig.set("Player." + playerUU + ".PV", PV);
		try {
			playerConfig.save(playerFile);
		} catch (IOException e) {
			Bukkit.getLogger().log(Level.WARNING, "Could not save " + player.getName() + "'s stats. [PrimeBall]");
		}
		

		if (player.isOnline()) {
			if (player.hasPermission("fb.extrapv")) {
				PV = PV + PrimeBallMain.plugin.getConfig().getDouble("extraPV");
			}
		}
		return;
		
	}
	
	public File getPlayerFile(Player player) {
		String playerUU = player.getUniqueId().toString();
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/PlayerStats/");
		File f = new File(folder, File.separator + playerUU + ".yml");
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return f;
	}
	
	
	public double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();
	    if (value < 0.1) return 0.1;
	    
	    try {
	    	BigDecimal bd = new BigDecimal(value);
	    	bd = bd.setScale(places, RoundingMode.HALF_UP);
	    	return bd.doubleValue();
	    } catch (NumberFormatException NFE) {
	    	return 0;
	    }
	}
	
}
