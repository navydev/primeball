package me.NavyDev.PlayerStats;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import me.navy12333.PrimeBall.PrimeBallMain;

public class PlayerOwnGoal {

	public PlayerOwnGoal(Player player) {
		File playerFile = getPlayerFile(player);
		FileConfiguration playerConfig = YamlConfiguration.loadConfiguration(playerFile);
		String id = player.getUniqueId().toString();
		if (playerConfig.getConfigurationSection("Player.") != null) {
			int OG = playerConfig.getInt("Player." + id + ".OG");
			playerConfig.set("Player." + id + ".OG", OG + 1);
		} else {
			playerConfig.set("Player." + id + ".MP", 0);
			playerConfig.set("Player." + id + ".MW", 0);
			playerConfig.set("Player." + id + ".ML", 0);
			playerConfig.set("Player." + id + ".MD", 0);
			playerConfig.set("Player." + id + ".GS", 0);
			playerConfig.set("Player." + id + ".AS", 0);
			playerConfig.set("Player." + id + ".OG", 1);
			playerConfig.set("Player." + id + ".PN", 0);
			playerConfig.set("Player." + id + ".PO", 0);
		}
		int OG = playerConfig.getInt("Player." + id + ".OG");
		player.sendMessage(" ");
		player.sendMessage(ChatColor.WHITE + "[" + ChatColor.BLUE + "Agent" + ChatColor.WHITE + "]"
				+ ChatColor.GOLD + " *Sigh* Unlucky on that own goal there mate... Unfortunately"
						+ " I still need to add it to the tally... You've now scored: " + OG +
						" Own goals.");
		
		try {
			playerConfig.save(playerFile);
		} catch (IOException e) {
			
		}
	}
	
	
	public File getPlayerFile(Player player) {
		String playerUU = player.getUniqueId().toString();
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/PlayerStats/");
		File f = new File(folder, File.separator + playerUU + ".yml");
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return f;
	}
	
}
