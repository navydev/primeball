package me.NavyDev.Commands;

import org.bukkit.entity.Player;

import me.NavyDev.Arena.CheckExistsArena;
import me.NavyDev.Arena.DeleteArena;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;

public class CmdDeleteArena {

	public CmdDeleteArena(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("arena")) {
				if (player.hasPermission("fb.arena")) {
					if (args.length > 1) {
						if (args[1].equalsIgnoreCase("delete")) {
							String arenaName = String.valueOf(args[2]).toUpperCase();
							boolean exists = new CheckExistsArena().execute(arenaName);
							if (!exists) {
								new SendColourMessageUtil(player, "&cWe're sorry but that arena does not exist. Try a different name!");
							} else {
								new DeleteArena(arenaName);
								new SendColourMessageUtil(player, "&bYou have deleted the arena &7" + arenaName);
							}
						}
					}
				} else {
					new SendColourMessageUtil(player, "&cWe're sorry but you do not have the permission needed to use this command.");
				}
			}
		}
		return;
	}
}
