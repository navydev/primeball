package me.NavyDev.Commands;

import org.bukkit.entity.Player;

import me.NavyDev.Match.Match;
import me.NavyDev.Match.MatchHandler;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;
import me.navy12333.PrimeBall.PrimeBallMain;

public class CmdForceEnd {

	public CmdForceEnd(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("forceend")) {
				if (player.hasPermission("fb.forceend")) {	
					if (args.length > 1) {
						String arenaName = args[1].toUpperCase();
						MatchHandler MH = PrimeBallMain.plugin.MH;
						if (MH.getStringMatches().containsKey(arenaName)) {
							Match match = MH.getStringMatches().get(arenaName);
							match.onEnd();
							new SendColourMessageUtil(player, "&aThe arena has been forced to end"); 
						} else {
							new SendColourMessageUtil(player, "&cThat arena is not running.");
						}
					} else {
						new SendColourMessageUtil(player, "&cPlease supply an arena name with this command.");
					}
				} else {
					new SendColourMessageUtil(player, "&cYou do not have the permission for this command.");
				}
			}
		}
		return;
	}
}
