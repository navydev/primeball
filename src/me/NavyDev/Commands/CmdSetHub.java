package me.NavyDev.Commands;

import org.bukkit.entity.Player;

import me.NavyDev.PrimeBallMain.Utils.LocationToString;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;
import me.navy12333.PrimeBall.PrimeBallMain;

public class CmdSetHub {

	public CmdSetHub(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("hub")) {
				if (player.hasPermission("fb.hub")) {
					String loc = new LocationToString().execute(player.getLocation());
					PrimeBallMain.plugin.getConfig().set("hubLocation", loc);
					PrimeBallMain.plugin.saveConfig();
					new SendColourMessageUtil(player, "&bYou have set the hub for all arenas.");
				
				} else {
					new SendColourMessageUtil(player, "&cWe're sorry but you do not have the permission needed to use this command.");
				}
			}
		}
		return;
	}
}
