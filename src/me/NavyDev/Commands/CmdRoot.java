package me.NavyDev.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.navy12333.PrimeBall.PrimeBallMain;

public class CmdRoot implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if (PrimeBallMain.plugin.MH.getPlayers().contains(player) && !isLeaveCommand(args) && !player.hasPermission("fb.bypasscmds")) {
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&0[&6PrimeBall&0] &6Sorry, you cannot run "
						+ "commands while playing a match."));
			} else {
				if (alias.equalsIgnoreCase("fb")) {
					new CmdCreateArena(player, args);
					new CmdDeleteArena(player, args);
					new CmdDespawnBall(player, args);
					new CmdFbStats(player, args);
					new CmdForceEnd(player, args);
					new CmdForceStart(player, args);
					new CmdGiveBoost(player, args);
					new CmdHelp(player, args);
					new CmdLeave(player, args);
					new CmdPlayerJoin(player, args);
					new CmdReloadArena(player, args);
					new CmdReloadConfig(player, args);
					new CmdSetBall(player, args);
					new CmdSetBallSpawnArena(player, args);
					new CmdSetPower(player, args);
					new CmdSetRunTimeArena(player, args);
					new CmdSetTeamOneSpawnArena(player, args);
					new CmdSetTeamTwoSpawnArena(player, args);
					new CmdSpawnBall(player, args);
					new CmdSetHub(player, args);
					new CmdSetPhysicsTypeArena(player, args);
					new CmdArenaInfo(player, args);
					new CmdArenas(player, args);
				}
			}
		}
		return false;
	}
	
	public boolean isLeaveCommand(String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("leave")) return true;
			if (args[0].equalsIgnoreCase("l")) return true;
			return false;
		} else {
			return false;
		}
	}

}
