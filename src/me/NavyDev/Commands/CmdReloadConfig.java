package me.NavyDev.Commands;

import org.bukkit.entity.Player;

import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;
import me.navy12333.PrimeBall.PrimeBallMain;

public class CmdReloadConfig {

	public CmdReloadConfig(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("config")) {
				if (player.hasPermission("fb.config")) {
					if (args.length > 0) {
						if (args[1].equalsIgnoreCase("reload")) {
							PrimeBallMain.plugin.reloadConfig();
							new SendColourMessageUtil(player, "&bConfig.yml reloaded");
						}
					} else {
						new SendColourMessageUtil(player, "&c/fb config reload");
					}
				} else {
					new SendColourMessageUtil(player, "&cSorry, you do not have the permission for that command.");
				}
			}
		}
		return;
	}

}
