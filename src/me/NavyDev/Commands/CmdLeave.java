package me.NavyDev.Commands;

import org.bukkit.entity.Player;

import me.NavyDev.Match.Match;
import me.NavyDev.Match.MatchHandler;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;
import me.navy12333.PrimeBall.PrimeBallMain;

public class CmdLeave {

	public CmdLeave(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("leave")) {
				if (player.hasPermission("fb.leave")) {
					MatchHandler MH = PrimeBallMain.plugin.MH;
					if (MH.getPlayerMatches().containsKey(player)) {
						Match match = MH.getPlayerMatches().get(player);
						match.playerLeave(player);
						new SendColourMessageUtil(player, "&cYou have left the arena " + match.getMatchName());
					} else {
						new SendColourMessageUtil(player, "&cYou're currently not in an arena");
					}
				} else {
					new SendColourMessageUtil(player, "&cYou do not have the permission for this command.");
				}
			} else if (args[0].equalsIgnoreCase("l")) {
				if (player.hasPermission("fb.leave")) {
					MatchHandler MH = PrimeBallMain.plugin.MH;
					if (MH.getPlayerMatches() != null) {
						if (MH.getPlayerMatches().containsKey(player)) {
							Match match = MH.getPlayerMatches().get(player);
							match.playerLeave(player);
							new SendColourMessageUtil(player, "&cYou have left the arena " + match.getMatchName());
						} else {
							new SendColourMessageUtil(player, "&cYou're currently not in an arena");
						}
					}
				} else {
					new SendColourMessageUtil(player, "&cYou do not have the permission for this command.");
				}
			}
		}
		return;
	}
}
