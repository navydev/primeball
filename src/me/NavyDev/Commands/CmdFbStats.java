package me.NavyDev.Commands;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;
import me.NavyDev.Scoreboards.StatsScoreboard;
import me.navy12333.PrimeBall.PrimeBallMain;

public class CmdFbStats {

	public CmdFbStats(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("stats")) {
				if (player.hasPermission("fb.stats")) {				
					File playerFile = getPlayerFile(player);
					FileConfiguration playerConfig = YamlConfiguration.loadConfiguration(playerFile);
					if (playerConfig.getConfigurationSection("Player.") != null) {
						new StatsScoreboard(player);								
					} else {
						new SendColourMessageUtil(player, "&cPlease play a match first.");
					}
				} else {
					new SendColourMessageUtil(player, "&cYou do not have the permission for this command.");
				}
			}
		}
		return ;
	}
	
	public File getPlayerFile(Player player) {
		String playerUU = player.getUniqueId().toString();
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/PlayerStats/");
		File f = new File(folder, File.separator + playerUU + ".yml");

		return f;
	}

}
