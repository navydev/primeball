package me.NavyDev.Commands;

import org.bukkit.entity.Player;

import me.NavyDev.Arena.CheckExistsArena;
import me.NavyDev.Arena.CreateArena;
import me.NavyDev.PrimeBallMain.Utils.LocationToString;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;

public class CmdCreateArena {
	
	public CmdCreateArena(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("arena")) {
				if (player.hasPermission("fb.arena")) {
					if (args.length > 1) {
						if (args[1].equalsIgnoreCase("create")) {
							String arenaName = String.valueOf(args[2]).toUpperCase();
							if (arenaName.length() < 13) {
								boolean exists = new CheckExistsArena().execute(arenaName);
								if (exists) {
									new SendColourMessageUtil(player, "&cWe're sorry but that arena already exists. Try a different name!");
								} else {
									String loc = new LocationToString().execute(player.getLocation());
									new CreateArena(arenaName, loc);
									new SendColourMessageUtil(player, "&bYou have created the arena &7" + arenaName + "&b, the ball's spawn has"
											+ " been set at your location. ");
								}
							} else {
								new SendColourMessageUtil(player, "&cWe're sorry but the arena name cannot be longer than 14 characters long.");
							}
						}
					} else {
						new SendColourMessageUtil(player, "&cWe're sorry but to use the arena command we need more arguments."
								+ " Use &7/fb help arena&cfor more information.");
					}
				} else {
					new SendColourMessageUtil(player, "&cWe're sorry but you do not have the permission needed to use this command.");
				}
			}
		}
		return;
	}
}
