package me.NavyDev.Commands;

import org.bukkit.entity.Player;

import me.NavyDev.Arena.CheckExistsArena;
import me.NavyDev.Arena.SetPhysicsArena;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;

public class CmdSetPhysicsTypeArena {

	public CmdSetPhysicsTypeArena(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("arena")) {
				if (player.hasPermission("fb.arena")) {
					if (args.length > 3) {
						if (args[1].equalsIgnoreCase("physics")) {
							String arenaName = String.valueOf(args[2]).toUpperCase();
							boolean exists = new CheckExistsArena().execute(arenaName);
							if (!exists) {
								new SendColourMessageUtil(player, "&cWe're sorry but that arena does not exist. Please use &7/fb arena create"
										+ " [arenaName]");
							} else {
								new SetPhysicsArena(arenaName, args[3]);
								new SendColourMessageUtil(player, "&bYou have set the physics type for the " + arenaName);
							}
						}
					}
				} else {
					new SendColourMessageUtil(player, "&cWe're sorry but you do not have the permission needed to use this command.");
				}
			}
		}
		return;
	}
}
