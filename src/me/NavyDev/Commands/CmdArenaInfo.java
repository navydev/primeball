package me.NavyDev.Commands;

import java.io.File;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import me.NavyDev.Match.Match;
import me.NavyDev.Match.MatchHandler;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;
import me.navy12333.PrimeBall.PrimeBallMain;

public class CmdArenaInfo {

	public CmdArenaInfo(Player player, String[] args) {
		if (args.length == 2) {
			if (args[0].equalsIgnoreCase("info")) {
				if (player.hasPermission("fb.info")) {
						String arenaName = args[1];
						PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
						File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/");
						File[] files = folder.listFiles();
						String path = Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/";
						if (folder.listFiles() != null) {
							if (files.length > 0) {
								for (File file : files) {
									String fileArenaName = file.getName().substring(0, file.getName().length() - 4).replaceAll(path, "");
									if (fileArenaName.equalsIgnoreCase(arenaName)) doInfo(player, fileArenaName);
								}
							} else {
								new SendColourMessageUtil(player, "&cArena does not exist");
							}
						} else {
							new SendColourMessageUtil(player, "&cArena does not exist");
						}
				} else {
					new SendColourMessageUtil(player, "&cSorry you do not have the permission to run that command.");
				}
			}
		}
	}
	
	public void doInfo(Player player, String arenaName) {
		MatchHandler MH = PrimeBallMain.plugin.MH;
		if (MH.getStringMatches().containsKey(arenaName)) {
			Match match = MH.getStringMatches().get(arenaName);
			if (match.getStage().equals(Match.stages.ACTIVE)) {
				new SendColourMessageUtil(player, "&7&m----------&6 Arena: " +  arenaName + "&7 &m----------");
				new SendColourMessageUtil(player, "&7- &aLast Goal Scorer: &7" + match.getLastGoalScorer());
				new SendColourMessageUtil(player, "&7- &bSco&cre: " + match.getScore());
				new SendColourMessageUtil(player, "&7- &cPlayers: &7" + match.getAmountPlayers());
				new SendColourMessageUtil(player, "&7- &4Playing: &7" + match.getAmountPlaying());
				new SendColourMessageUtil(player, "&7- &6Time Left: &7" + match.getTimeLeft());
				String goalScorers = "";
				HashMap<UUID, Integer> goals = new HashMap<UUID, Integer>();
				goals.putAll(match.getPlayerGoals());
				for (UUID ID : goals.keySet()) {
					Player player2 = Bukkit.getPlayer(ID);
					goalScorers = goalScorers + "&7" + player2.getName() + ": " + goals.get(ID) + "&6, ";
				}
				new SendColourMessageUtil(player, "&9&m----------&7 Goals &9&m----------");
				new SendColourMessageUtil(player, goalScorers);
				goals.clear();
				goals.putAll(match.getPlayerOwnGoals());
				goalScorers = "";
				for (UUID ID : goals.keySet()) {
					Player player2 = Bukkit.getPlayer(ID);
					goalScorers = goalScorers + "&7" + player2.getName() + ": " + goals.get(ID) + "&6, ";
				}
				new SendColourMessageUtil(player, "&9&m--------&c Own Goals &9&m--------");
				new SendColourMessageUtil(player, goalScorers);
				new SendColourMessageUtil(player, "&7&m----------&6 Arena: " +  arenaName + " END&7 &m----------");
			} else {
				new SendColourMessageUtil(player, "This arena is not active.");
			}
		}
	}
}
