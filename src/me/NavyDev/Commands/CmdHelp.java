package me.NavyDev.Commands;

import org.bukkit.entity.Player;

import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;

public class CmdHelp {

	public CmdHelp(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("help")) {
				if (player.hasPermission("fb.help")) {
					if (args.length > 1) {
						if (args[1].equalsIgnoreCase("arena")) {
							new SendColourMessageUtil(player, "&7&m----------&6 &nPrimeball Arena Help&7 &m----------");
							new SendColourMessageUtil(player, "&7+ &6/fb arena create [arenaName] &7- Creates your arena");
							new SendColourMessageUtil(player, "&7+ &6/fb arena red [arenaName] &7- Sets the spawn for team RED");
							new SendColourMessageUtil(player, "&7+ &6/fb arena blue [arenaName] &7- Sets the spawn for team BLUE");
							new SendColourMessageUtil(player, "&7+ &6/fb arena ball [arenaName] &7- Sets the spawn of the BALL");
							new SendColourMessageUtil(player, "&7+ &6/fb arena physics [arenaName] {classic, navySpecial}");
							new SendColourMessageUtil(player, "&7+ &6/fb arena runTime [time] [arenaName] &7- Sets the length the"
									+ " match will run for (seconds)");
							new SendColourMessageUtil(player, "&7+ &6/fb arena reload [arenaName] &7- Reloads the data of the arena");
							new SendColourMessageUtil(player, "&7+ &6/fb arena delete [arenaName] &7- Deletes the arena");
							new SendColourMessageUtil(player, "&7+ &6/fb arenas [active/all] &7- Lists arenas");
							new SendColourMessageUtil(player, "&7+ &6/fb info [arenaName] &7- See the info of an active arena");
						} else if (args[1].equalsIgnoreCase("all")) {
							new SendColourMessageUtil(player, "&7&m----------&6 &nPrimeball All Help&7 &m----------");
							new SendColourMessageUtil(player, "&7+ &6/fb arena &7- Main arena command.");
							new SendColourMessageUtil(player, "&7+ &6/fb ball &7- Set the ball type for all matches");
							new SendColourMessageUtil(player, "&7+ &6/fb power [int] &7- Set the kicking power for all matches");
							new SendColourMessageUtil(player, "&7+ &6/fb spawnball &7- Spawns a ball at your location for money");
							new SendColourMessageUtil(player, "&7+ &6/fb despawnball &7- Removes balls in a 1x1x1 area");
							new SendColourMessageUtil(player, "&7+ &6/fb boost [type] [amount] [player] &7- Gives boost");
							new SendColourMessageUtil(player, "&7+ &6/fb stats &7- Shows your stats");
							new SendColourMessageUtil(player, "&7+ &6/fb join [arena] {team} &7- Join an arena");
							new SendColourMessageUtil(player, "&7+ &6/fb join &7- Join a random arena");
							new SendColourMessageUtil(player, "&7+ &6/fb leave &7- Leave your current arena");
							new SendColourMessageUtil(player, "&7+ &6/fb help {arena, all} &7- displays help pages");
							new SendColourMessageUtil(player, "&7+ &6/fb forcestart [arenaName] &7- Force starts an arena");
							new SendColourMessageUtil(player, "&7+ &6/fb forceend [arenaName] &7- Force ends an arena");
						}
					} else {
						new SendColourMessageUtil(player, "&7Which help page would you like? &cArena &7or &cAll?");
					}
				} else {
					new SendColourMessageUtil(player, "&cYou do not have the permission for this command.");
				}
			}
		}
		return;
	}
}
