package me.NavyDev.Commands;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import me.NavyDev.Match.Match;
import me.NavyDev.Match.MatchHandler;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;
import me.navy12333.PrimeBall.PrimeBallMain;

public class CmdArenas {

	public CmdArenas(Player player, String[] args) {
		if (args.length == 2) {
			if (args[0].equalsIgnoreCase("arenas")) {
				if (player.hasPermission("fb.arenas")) {
					String choice = args[1];
					if (choice.equalsIgnoreCase("active")) {
						doActiveArenas(player);
					} else if (choice.equalsIgnoreCase("all")) {
						doAllArenas(player);
					} else {
						new SendColourMessageUtil(player, "&cIncorrect arguments (&7/fb arenas [active/all]&c)");
					}
				} else {
					new SendColourMessageUtil(player, "&cSorry you do not have the permission to run that command.");
				}
			}
		}
	}
	public void doAllArenas(Player player) {
		MatchHandler MH = PrimeBallMain.plugin.MH;
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/");
		File[] files = folder.listFiles();
		String path = Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/";
		if (folder.listFiles() != null) {
			new SendColourMessageUtil(player, "&7&m----------&6 Primeball All Arenas&7 &m----------");
			if (files.length > 0) {
				for (File file : files) {
					String arenaName = file.getName().substring(0, file.getName().length() - 4).replaceAll(path, "");
					if (MH.getStringMatches().containsKey(arenaName)) {
						Match match = MH.getStringMatches().get(arenaName);
						if (match.getStage().equals(Match.stages.ACTIVE)) {
							new SendColourMessageUtil(player, "&7- &a" + match.getMatchName() + "&7 - "
									+ "&6Stage: &a&lACTIVE");
						} else if (match.getStage().equals(Match.stages.WAITING)) {
							new SendColourMessageUtil(player, "&7- &a" + match.getMatchName() + "&7 - "
									+ "&6Stage: &7&lWAITING");	
						} else {
							new SendColourMessageUtil(player, "&7- &a" + match.getMatchName() + "&7 - "
									+ "&6Stage: &7 " + match.getStage());	
						}
					} else {
						new SendColourMessageUtil(player, "&7- &n" + arenaName + "&7 - "
								+ "Stage: &7N/A");
					}
				}
			} else {
				new SendColourMessageUtil(player, "&6No arenas made.");
			}
			new SendColourMessageUtil(player, "&7&m---------&6 Primeball All Arenas End&7 &m--------");
		}
	}
	
	public void doActiveArenas(Player player) {
		MatchHandler MH = PrimeBallMain.plugin.MH;
		ArrayList<Match> matches = new ArrayList<Match>();
		if (MH.getMatches() != null && !MH.getMatches().isEmpty()) {
			matches.addAll(MH.getMatches());
			new SendColourMessageUtil(player, "&7&m----------&6 Primeball Active Arenas&7 &m----------");
			for (Match match : matches) {
				if (match.getStage().equals(Match.stages.ACTIVE)) {
					new SendColourMessageUtil(player, "&7- &aMatch Name: &n" + match.getMatchName() + "&7 - "
							+ "&6Stage: &a&lACTIVE &7- &5Players: " + match.getAmountPlaying() +
							"&7 - &3Time Left: &4" + match.getTimeLeft() + " (S)");
				} else if (match.getStage().equals(Match.stages.WAITING)) {
					new SendColourMessageUtil(player, "&7- &aMatch Name: &n" + match.getMatchName() + "&7 - "
							+ "&6Stage: &7&lWAITING &7- &5Players Waiting: " + match.getAmountPlayers() +
							"&7 - &3Till Start: &4" + match.getTimeUntilStart() + " (S)");	
				} else {
					new SendColourMessageUtil(player, "&7- &aMatch Name: &n" + match.getMatchName() + "&7 - "
							+ "&6Stage: &7 " + match.getStage() + "&7- &5Players: " + match.getAmountPlayers());	
				}
			}
			new SendColourMessageUtil(player, "&7&m---------&6 Primeball Active Arenas End&7 &m--------");
		} else {
			new SendColourMessageUtil(player, "&7&m----------&6 Primeball Active Arenas&7 &m----------");
			new SendColourMessageUtil(player, "&6No arenas active.");
			new SendColourMessageUtil(player, "&7&m---------&6 Primeball Active Arenas End&7 &m--------");
		}
	}
}
