package me.NavyDev.Commands;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.NavyDev.MultiVersion.CheckItemInHandExistUtil;
import me.NavyDev.MultiVersion.GetClientVersionUtil;
import me.NavyDev.MultiVersion.GetPostOneNineItemInHand;
import me.NavyDev.MultiVersion.GetPreOneNineItemInHand;
import me.NavyDev.MultiVersion.Version;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;
import me.navy12333.PrimeBall.PrimeBallMain;

public class CmdSetBall {

	public CmdSetBall(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("ball")) {
				if (player.hasPermission("fb.ball")) {
					if (!new CheckItemInHandExistUtil().execute(player)) {
						Version serverVersion = new GetClientVersionUtil().execute();
						ItemStack hand;
						if (serverVersion.equals(Version.ONE_EIGHT) || serverVersion.equals(Version.ONE_NINE)) hand = new GetPreOneNineItemInHand().execute(player);
							else hand = new GetPostOneNineItemInHand().execute(player);
						ItemStack ItemInMainHand = hand;
						PrimeBallMain.plugin.getConfig().set("ball", ItemInMainHand);
						PrimeBallMain.plugin.saveConfig();
						if (ItemInMainHand.hasItemMeta() && ItemInMainHand.getItemMeta().hasDisplayName()) {
							String itemName = ItemInMainHand.getItemMeta().getDisplayName();
							new SendColourMessageUtil(player, "&aBall set to &7" + itemName);
						} else {
							new SendColourMessageUtil(player, "&aBall set to &7(no name found)" );
						}
					} else {
						new SendColourMessageUtil(player, "&cYou must be holding an item in your mainhand to set the ball.");
					}
				} else {
					new SendColourMessageUtil(player, "&cSorry, but you do not have the permission to run that command.");
				}
			}
		} else {
			new SendColourMessageUtil(player, "&cYou're missing arguements. Refer to the help page &7/fb help all");
		}
		return;
	}

}
