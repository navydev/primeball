package me.NavyDev.Commands;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;
import me.navy12333.PrimeBall.PrimeBallMain;

public class CmdSetPower {

	public CmdSetPower(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("power")) {
				if (player.hasPermission("fb.setpower")) {
					if (args.length > 1) {
						double power = Double.parseDouble(args[1]);
						if (power >= 3.0) {
							new SendColourMessageUtil(player, "&cYou must not enter a value over 3.0.");
						} else {
							new SendColourMessageUtil(player, "Power adjustment set to "
									+ round(power*100, 2) + "%");
							PrimeBallMain.plugin.getConfig().set("power", Double.valueOf(power));
							PrimeBallMain.plugin.saveConfig();
						}				
					}else {
						player.sendMessage(ChatColor.RED + "Please input a value.");
					}
				} else {
					new SendColourMessageUtil(player, "&cWe're sorry but you do not have the permission needed to use this command.");
				}
			}
		} 
		return;
	}
	
	public double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}

