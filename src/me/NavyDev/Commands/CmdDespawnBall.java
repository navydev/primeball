package me.NavyDev.Commands;

import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;
import me.navy12333.PrimeBall.PrimeBallMain;

public class CmdDespawnBall {

	public CmdDespawnBall(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("despawnball")) {
				if (player.hasPermission("fb.despawnball")) {
					List<Entity> ent = player.getNearbyEntities(1, 1, 1);
					for (Entity entity : ent) {
						if (entity instanceof Item) {
							UUID id = entity.getUniqueId();
							if (PrimeBallMain.plugin.BH.getBallIDs().contains(id)) {
								PrimeBallMain.plugin.BH.despawnBall(id);
								entity.remove();
							}
						}
					}
					new SendColourMessageUtil(player, "&cYou have despawned a ball.");
				} else {
					new SendColourMessageUtil(player, "&cSorry, but you do not have the permission to run that command.");
				}
			}
		}
		return;
	}

}
