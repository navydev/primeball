package me.NavyDev.Commands;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import me.NavyDev.Ball.Ball;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;
import me.navy12333.PrimeBall.PrimeBallMain;

public class CmdSpawnBall {

	public CmdSpawnBall(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("spawnball")) {
				if (player.hasPermission("fb.spawnball")) {	
					if (!PrimeBallMain.coolDown.containsKey(player)) {
						Location loc = player.getLocation();
						List<String> worlds = PrimeBallMain.plugin.getConfig().getStringList("trainingWorlds");
						
						if (worlds.contains(player.getWorld().getName())){
							double bal = PrimeBallMain.plugin.getEcon().getBalance(player);
							int price = 100;
							if (player.hasPermission("PrimeBall.cheapball")) {
								price = 50;
							}
							if (bal >= price) {
								new Ball().spawnBall(loc);
								PrimeBallMain.plugin.getEcon().withdrawPlayer(player, price);
								player.sendMessage("Ball spawned for " + PrimeBallMain.plugin.getEcon().format(price));
								PrimeBallMain.coolDown.put(player, 240);
							} else {
								new SendColourMessageUtil(player, "&cYou do not have enough money to spawn a ball. Sorry.");
							}
						} else {
							player.sendMessage("You must be in a training zone to run this command.");
						}
					} else {
						new SendColourMessageUtil(player, "&cYou have just spawned a ball. Please wait &7" + PrimeBallMain.coolDown.get(player) + 
								" &cseconds before spawning another.");
					}
				} else {
					new SendColourMessageUtil(player, "&cWe're sorry but you do not have the permission needed to use this command.");
				}
			}
		}
		return;
	}
}
