package me.NavyDev.Commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.NavyDev.PrimeBallMain.Utils.GetBoosterUtil;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;

public class CmdGiveBoost {

	public CmdGiveBoost(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("boost")) {
				if (player.hasPermission("fb.boost")) {
					if (args.length > 2) {
						String type = args[1];
						int amount = Integer.valueOf(args[2]);
						if (amount == 0) {
							new SendColourMessageUtil(player, "&cThe amount must be greater than zero.");
							return;
						}
						Player boostReceiver;
						if (Bukkit.getPlayer(args[3]) != null) {
							boostReceiver = Bukkit.getPlayer(args[3]);
						} else {
							new SendColourMessageUtil(player, "&cWe could not find that player.");
							return;
						}
						ArrayList<String> lore = new ArrayList<String>();
						if (type.equals("5")) {
							lore.add(ChatColor.BOLD + "5% PV and Cash Boost!");
							ItemStack five = new GetBoosterUtil().execute("&b&l5% Booster", 
									Material.STAINED_CLAY, amount, (short) 11, lore);
							boostReceiver.getInventory().addItem(five);
							new SendColourMessageUtil(boostReceiver, "&bYou have received 5% boosters");
							new SendColourMessageUtil(player, "&cWe have sent the boosts.");
						} else if (type.equals("10")) {
							lore.add(ChatColor.BOLD + "10% PV and Cash Boost!");
							ItemStack ten =  new GetBoosterUtil().execute("&b&l10% Booster", 
									Material.STAINED_CLAY, amount, (short) 4, lore);
							boostReceiver.getInventory().addItem(ten);
							new SendColourMessageUtil(boostReceiver, "&bYou have received 10% boosters");
							new SendColourMessageUtil(player, "&cWe have sent the boosts.");
						} else if (type.equals("25")) {
							lore.add(ChatColor.BOLD + "25% PV and Cash Boost!");
							ItemStack tfive = new GetBoosterUtil().execute("&b&l25% Booster",
									Material.STAINED_CLAY, amount, (short) 1, lore);
							boostReceiver.getInventory().addItem(tfive);
							new SendColourMessageUtil(boostReceiver, "&bYou have received 25% boosters");
							new SendColourMessageUtil(player, "&cWe have sent the boosts.");
						} else if (type.equals("50")) {
							lore.add(ChatColor.BOLD + "50% PV and Cash Boost!");
							ItemStack fifty = new GetBoosterUtil().execute("&b&l25% Booster",
									Material.STAINED_CLAY, amount, (short) 14, lore);
							boostReceiver.getInventory().addItem(fifty);
							new SendColourMessageUtil(boostReceiver, "&bYou have received 50% boosters");
							new SendColourMessageUtil(player, "&cWe have sent the boosts.");
						} else {						
							new SendColourMessageUtil(player, "&cWe could not decide what boost you wanted.");
							return;
						}
					}
				} else {
					new SendColourMessageUtil(player, "&cYou do not have the permission for this command.");
				}
			}
		}
		return;
	}

}
