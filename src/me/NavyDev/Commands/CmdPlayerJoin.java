package me.NavyDev.Commands;

import java.io.File;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import me.NavyDev.Match.Match;
import me.NavyDev.Match.MatchHandler;
import me.NavyDev.Match.Match.stages;
import me.NavyDev.PrimeBallMain.Utils.LocationFromStringUtil;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;
import me.navy12333.PrimeBall.PrimeBallMain;

public class CmdPlayerJoin{

	public CmdPlayerJoin(Player player, String[] args) {
		if (args.length > 0) {
			Location loc = new LocationFromStringUtil().execute(PrimeBallMain.plugin.getConfig().getString("hubLocation"));
			if (loc != null) {
				if (args.length == 1) {
					if (args[0].equalsIgnoreCase("join")) {
						if (player.hasPermission("fb.join")) {
							joinWithoutTeam(player, args);
						} else {
							new SendColourMessageUtil(player, "&cYou do not have the permission to run this command.");
						}
					} else if (args[0].equalsIgnoreCase("j")) {
						if (player.hasPermission("fb.join")) {
							joinWithoutTeam(player, args);
						} else {
							new SendColourMessageUtil(player, "&cYou do not have the permission to run this command.");
						}
					}
				} else if (args.length == 2) {
					if (args[0].equalsIgnoreCase("join")) {
						if (player.hasPermission("fb.join")) {
							joinWithoutTeam(player, args);
						} else {
							new SendColourMessageUtil(player, "&cYou do not have the permission to run this command.");
						}
					} else if (args[0].equalsIgnoreCase("j")) {
						if (player.hasPermission("fb.join")) {
							joinWithoutTeam(player, args);
						} else {
							new SendColourMessageUtil(player, "&cYou do not have the permission to run this command.");
						}
					}
				} else if (args.length == 3) {
					if (args[0].equalsIgnoreCase("join")) {
						if (player.hasPermission("fb.join")) {
							String teamSTR = args[2];
							Match.teams team = null;
							if (teamSTR.equalsIgnoreCase("red")) {
								team = Match.teams.TEAM_ONE;
								joinWithTeam(player, args, team);
							} else if (teamSTR.equalsIgnoreCase("blue")) {
								team = Match.teams.TEAM_TWO;
								joinWithTeam(player, args, team);
							} else joinWithoutTeam(player, args);
						} else {
							new SendColourMessageUtil(player, "&cYou do not have the permission to run this command.");
						}
					} else if (args[0].equalsIgnoreCase("j")) {
						if (player.hasPermission("fb.join")) {
							String teamSTR = args[2];
							Match.teams team = null;
							if (teamSTR.equalsIgnoreCase("red")) {
								team = Match.teams.TEAM_ONE;
								joinWithTeam(player, args, team);
							} else if (teamSTR.equalsIgnoreCase("blue")) {
								team = Match.teams.TEAM_TWO;
								joinWithTeam(player, args, team);
							} else joinWithoutTeam(player, args);
						} else {
							new SendColourMessageUtil(player, "&cYou do not have the permission to run this command.");
						}
					}
				}
			} else {
				new SendColourMessageUtil(player, "&cSorry game joining is disabled as the hub is not set. &7(/fb hub).");
			}
		}
		return;
	}
	

	public void joinWithTeam(Player player, String[] args, Match.teams team) {
		MatchHandler MH = PrimeBallMain.plugin.MH;
		if (!MH.getPlayerMatches().containsKey(player)) {
			if (args.length > 1) {
				String arenaName = args[1].toUpperCase();
				if (MH.getStringMatches().containsKey(arenaName)) {
					Match match = MH.getStringMatches().get(arenaName);
					if (!match.getStage().equals(stages.WAITING)) {
						if (!casualArena(match)) {
							match.playerLateJoin(player, null);
							new SendColourMessageUtil(player, "&aJoined the arena " + arenaName + " without a team as it has started and the arena is competitive.");
						} else {
							match.playerJoin(player, team);
							new SendColourMessageUtil(player, "&aJoined the arena " + arenaName);
						}
					} else {
						match.playerJoin(player, team);
						new SendColourMessageUtil(player, "&aJoined the arena " + arenaName);
					}
				} else {
					PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
					File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/");
					File f = new File(folder, File.separator + arenaName + ".yml");
					if (arenaReady(f)) {
						Match match = new Match(arenaName, MH);
						match.playerJoin(player, team);
						new SendColourMessageUtil(player, "&aJoined the arena " + arenaName);
					} else {
						new SendColourMessageUtil(player, "&aThe arena " + arenaName + " is not ready to be played in.");
					}
				}
			} else {
				if (MH.getMatches() != null) {
					if (!MH.getMatches().isEmpty()) {
						Random rand = new Random();
						int no = rand.nextInt(MH.getMatches().size());
						Match match = MH.getMatches().get(no);
						match.playerLateJoin(player, team);
						new SendColourMessageUtil(player, "&aJoined the arena " + match.getMatchName());
					} else {
						if (getArenaFile() != null) {
							File file = getArenaFile();
							if (arenaReady(file)) {
								PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
								String path = Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/";
								String arenaName = file.getName().substring(0, file.getName().length() - 4).replaceAll(path, "");
								Match match = new Match(arenaName, MH);
								match.playerJoin(player, team);
								new SendColourMessageUtil(player, "&aJoined the arena " + match.getMatchName());
							} else {
								new SendColourMessageUtil(player, "&cSorry, we could not find an arena for you. Try again in a while.");
							}
						} else {
							new SendColourMessageUtil(player, "&cSorry, there are currently no arenas to play in.");
						}
					}
				}
			}
		} else {
			new SendColourMessageUtil(player, "&cYou're already in a football match!");
		}
	}
	
	public void joinWithoutTeam(Player player, String[] args) {
		MatchHandler MH = PrimeBallMain.plugin.MH;
		if (!MH.getPlayerMatches().containsKey(player)) {
			if (args.length > 1) {
				String arenaName = args[1].toUpperCase();
				if (MH.getStringMatches().containsKey(arenaName)) {
					Match match = MH.getStringMatches().get(arenaName);
					if (!match.getStage().equals(stages.WAITING)) {
						match.playerLateJoin(player, null);
						new SendColourMessageUtil(player, "&aJoined the arena " + arenaName);
					} else {
						match.playerJoin(player, null);
						new SendColourMessageUtil(player, "&aJoined the arena " + arenaName);
					}
				} else {
					PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
					File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/");
					File f = new File(folder, File.separator + arenaName + ".yml");
					if (arenaReady(f)) {
						Match match = new Match(arenaName, MH);
						match.playerJoin(player, null);
						new SendColourMessageUtil(player, "&aJoined the arena " + arenaName);
					} else {
						new SendColourMessageUtil(player, "&aThe arena " + arenaName + " is not ready to be played in.");
					}
				}
			} else {
				if (MH.getMatches() != null) {
					if (!MH.getMatches().isEmpty()) {
						Random rand = new Random();
						int no = rand.nextInt(MH.getMatches().size());
						Match match = MH.getMatches().get(no);
						match.playerLateJoin(player, null);
						new SendColourMessageUtil(player, "&aJoined the arena " + match.getMatchName());
					} else {
						if (getArenaFile() != null) {
							File file = getArenaFile();
							if (arenaReady(file)) {
								PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
								String path = Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/";
								String arenaName = file.getName().substring(0, file.getName().length() - 4).replaceAll(path, "");
								Match match = new Match(arenaName, MH);
								match.playerJoin(player, null);
								new SendColourMessageUtil(player, "&aJoined the arena " + match.getMatchName());
							} else {
								new SendColourMessageUtil(player, "&cSorry, we could not find an arena for you. Try again in a while.");
							}
						} else {
							new SendColourMessageUtil(player, "&cSorry, there are currently no arenas to play in.");
						}
					}
				}
			}
		} else {
			new SendColourMessageUtil(player, "&cYou're already in a football match!");
		}
	}
	public File getArenaFile() {
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/");
		
		File[] files = folder.listFiles();
		Random rand = new Random();
		if (files.length > 0) {
			int no = rand.nextInt(files.length);
			File f = files[no];
			return f;
		}
		return null;
	}
	
	public boolean arenaReady(File file) {
		FileConfiguration arenaConfig = YamlConfiguration.loadConfiguration(file);
		
		if (arenaConfig != null) {
			if (arenaConfig.getBoolean("ArenaData.Data.ready")) {
				return true;
			} else {
				if (arenaConfig.getString("ArenaData.Spawns.ballSpawn") == null || arenaConfig.getString("ArenaData.Spawns.ballSpawn").equals("{}")) {
					return false;
				} else if (arenaConfig.getString("ArenaData.Spawns.teamOneSpawn") == null || arenaConfig.getString("ArenaData.Spawns.teamOneSpawn").equals("{}")) {
					return false;
				} else if (arenaConfig.getString("ArenaData.Spawns.teamTwoSpawn") == null || arenaConfig.getString("ArenaData.Spawns.teamTwoSpawn").equals("{}")) {
					return false;
				} else {
					arenaConfig.set("ArenaData.Data.ready", true);
					return true;
				}
			}
		} else {
			return false;
		}
	}

	public boolean casualArena(Match match) {
		String arenaName = match.getMatchName();
		if (PrimeBallMain.plugin.getConfig().getStringList("casualArenas") != null) {
			List<String> casualArenas = PrimeBallMain.plugin.getConfig().getStringList("casualArenas");
			if (casualArenas.contains(arenaName)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
}
