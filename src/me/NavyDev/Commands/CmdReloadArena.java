package me.NavyDev.Commands;

import org.bukkit.entity.Player;

import me.NavyDev.Arena.CheckExistsArena;
import me.NavyDev.Arena.ReloadArena;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;

public class CmdReloadArena {

	public CmdReloadArena(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("arena")) {
				if (player.hasPermission("fb.arena")) {
					if (args.length > 1) {
						if (args[1].equalsIgnoreCase("reload")) {
							String arenaName = String.valueOf(args[2]).toUpperCase();
							boolean exists = new CheckExistsArena().execute(arenaName);
							if (!exists) {
								new SendColourMessageUtil(player, "&cWe're sorry but that arena does not exist. Please use &7/fb arena create"
										+ " [arenaName]");
							} else {
								new ReloadArena(player, arenaName);
								new SendColourMessageUtil(player, "&bYou have reloaded the data for the arena &7" + arenaName);
							}
						}
					}
				} else {
						new SendColourMessageUtil(player, "&cWe're sorry but you do not have the permission needed to use this command.");
					}
				}
			}
		return;
	}
}
