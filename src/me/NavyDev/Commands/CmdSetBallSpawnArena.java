package me.NavyDev.Commands;

import org.bukkit.entity.Player;

import me.NavyDev.Arena.BallSetArena;
import me.NavyDev.Arena.CheckExistsArena;
import me.NavyDev.PrimeBallMain.Utils.LocationToString;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;

public class CmdSetBallSpawnArena {

	public CmdSetBallSpawnArena(Player player, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("arena")) {
				if (player.hasPermission("fb.arena")) {
					if (args.length > 1) {
						if (args[1].equalsIgnoreCase("ball")) {
							String arenaName = String.valueOf(args[2]).toUpperCase();
							boolean exists = new CheckExistsArena().execute(arenaName);
							if (!exists) {
								new SendColourMessageUtil(player, "&cWe're sorry but that arena does not exist. Please use &7/fb arena create"
										+ " [arenaName]");
							} else {
								String loc = new LocationToString().execute(player.getLocation());
								new BallSetArena(arenaName, loc);
								new SendColourMessageUtil(player, "&bYou have set the spawn of the &fBALL &bin the arena &7" + arenaName);
							}
						}
					}
				} else {
					new SendColourMessageUtil(player, "&cWe're sorry but you do not have the permission needed to use this command.");
				}
			}
		}
		return;
	}
}
