package me.NavyDev.MultiVersion;

import org.bukkit.entity.Player;

public class GetPreOneNineItemInHand {

	@SuppressWarnings("deprecation")
	public org.bukkit.inventory.ItemStack execute(Player player) {
		if (player == null) return null;
		if (!player.isOnline()) return null;
		if (player.getItemInHand() == null) return null;
		
		return player.getItemInHand();
	}
}
