package me.NavyDev.MultiVersion;

public enum Version {
	ONE_EIGHT(),
	ONE_NINE(),
	ONE_TEN(),
	ONE_ELEVEN(),
	ONE_TWELVE(),
	UNSUPPORTED();
}
