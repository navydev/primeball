package me.NavyDev.MultiVersion;

import org.bukkit.Bukkit;

public class GetClientVersionUtil {
	
	public Version execute() {
		if (Bukkit.getVersion().contains("1.8")) return Version.ONE_EIGHT;
		if (Bukkit.getVersion().contains("1.9")) return Version.ONE_NINE;
		if (Bukkit.getVersion().contains("1.10")) return Version.ONE_TEN;
		if (Bukkit.getVersion().contains("1.11")) return Version.ONE_ELEVEN;
		if (Bukkit.getVersion().contains("1.12")) return Version.ONE_TWELVE; 
		else return Version.UNSUPPORTED;
	}
}
