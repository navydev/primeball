package me.NavyDev.MultiVersion;

import org.bukkit.entity.Player;

public class CheckItemInHandExistUtil {

	public boolean execute(Player player) {
		Version serverVersion = new GetClientVersionUtil().execute();
		if (serverVersion.equals(Version.ONE_EIGHT) || serverVersion.equals(Version.ONE_NINE)) {
			if (new GetPreOneNineItemInHand().execute(player) != null) return true; else return false;
		} else {
			if (new GetPostOneNineItemInHand().execute(player) != null) return true; else return false;	
		}
	}
}
