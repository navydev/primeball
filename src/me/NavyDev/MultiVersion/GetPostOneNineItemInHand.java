package me.NavyDev.MultiVersion;

import org.bukkit.entity.Player;

public class GetPostOneNineItemInHand {

	public org.bukkit.inventory.ItemStack execute(Player player) {
		if (player == null) return null;
		if (!player.isOnline()) return null;
		if (player.getInventory().getItemInMainHand() == null) return null;
		
		return player.getInventory().getItemInMainHand();
	}
	
}
