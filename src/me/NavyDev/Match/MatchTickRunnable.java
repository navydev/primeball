package me.NavyDev.Match;

import java.util.ArrayList;

import me.navy12333.PrimeBall.PrimeBallMain;

public class MatchTickRunnable implements Runnable {

	@Override
	public void run() {
		MatchHandler MH = PrimeBallMain.plugin.MH;
		if (MH.getMatches() != null) {
			if (!MH.getMatches().isEmpty()) {
				ArrayList<Match> matches = new ArrayList<Match>();
				matches.addAll(MH.getMatches());
				for (Match match : matches) {
					match.checkBall();
				}
			}
		}		
	}

}
