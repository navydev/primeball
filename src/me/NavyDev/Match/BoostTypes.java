package me.NavyDev.Match;

public enum BoostTypes {

	FIVE_BOOST(),
	TEN_BOOST(),
	TWENTY_FIVE_BOOST(),
	FIFTY_BOOST();
}
