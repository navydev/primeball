package me.NavyDev.Match;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;

import me.navy12333.PrimeBall.PrimeBallMain;

public class Boosts {

	public void fiveBoost(Player player) {
		File playerFile = getPlayerFile(player);
		FileConfiguration playerConfig = YamlConfiguration.loadConfiguration(playerFile);
		
		String playerUU = player.getUniqueId().toString();
		double PV = playerConfig.getDouble("Player." + playerUU + ".PV");
		double pPV = (PV * 0.05);
		PV = PV + pPV;
		pPV = round(PV, 2);
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + player.getName() + " 2.50");
		saveFile(playerFile, playerConfig, player);
	}
	
	public void tenBoost(Player player) {
		File playerFile = getPlayerFile(player);
		FileConfiguration playerConfig = YamlConfiguration.loadConfiguration(playerFile);
		
		String playerUU = player.getUniqueId().toString();
		double PV = playerConfig.getDouble("Player." + playerUU + ".PV");
		double pPV = (PV * 0.10);
		PV = PV + pPV;
		pPV = round(PV, 2);
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + player.getName() + " 5.00");
		saveFile(playerFile, playerConfig, player);
	}
	
	public void twentyFiveBoost(Player player) {
		File playerFile = getPlayerFile(player);
		FileConfiguration playerConfig = YamlConfiguration.loadConfiguration(playerFile);
		
		String playerUU = player.getUniqueId().toString();
		double PV = playerConfig.getDouble("Player." + playerUU + ".PV");
		double pPV = (PV * 0.25);
		PV = PV + pPV;
		pPV = round(PV, 2);
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + player.getName() + " 12.50");
		saveFile(playerFile, playerConfig, player);
	}
	
	public void fiftyBoost(Player player) {
		File playerFile = getPlayerFile(player);
		FileConfiguration playerConfig = YamlConfiguration.loadConfiguration(playerFile);
		
		String playerUU = player.getUniqueId().toString();
		double PV = playerConfig.getDouble("Player." + playerUU + ".PV");
		double pPV = (PV * 0.50);
		PV = PV + pPV;
		pPV = round(PV, 2);
		
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give " + player.getName() + " 25.00");
		saveFile(playerFile, playerConfig, player);
	}

	public File getPlayerFile(Player player) {
		String playerUU = player.getUniqueId().toString();
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/PlayerStats/");
		File f = new File(folder, File.separator + playerUU + ".yml");

		return f;
	}
	
	public double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();
	
	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public void saveFile(File playerFile, FileConfiguration playerConfig, Player player) {
		try {
			playerConfig.save(playerFile);
		} catch (IOException e) {
			Bukkit.getLogger().log(Level.WARNING, "Could not save " + player.getName() + "'s stats. [PrimeBall]");
		}
	}
}
