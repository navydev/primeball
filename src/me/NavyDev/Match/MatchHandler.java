package me.NavyDev.Match;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.entity.Player;

public class MatchHandler {

	private ArrayList<Player> allPlayers = new ArrayList<Player>();
	private ArrayList<Match> runningMatches = new ArrayList<Match>();
	private HashMap<String, Match> stringMatches = new HashMap<String, Match>();
	private HashMap<Player, Match> playerMatches = new HashMap<Player, Match>();
	
	public void matchTick() {
		if (this.runningMatches != null) {
			if (!this.runningMatches.isEmpty()) {
				ArrayList<Match> matches = new ArrayList<Match>();
				matches.addAll(this.runningMatches);
				for (Match match : matches) {
					match.tick();
				}
			}
		}
	}
	
	public void newMatch(String arenaName, Match match) {
		this.runningMatches.add(match);
		this.stringMatches.put(arenaName, match);
		return;
	}
	
	public void deleteMatch(String arenaName, Match match) {
		this.runningMatches.remove(match);
		this.stringMatches.remove(arenaName);
		return;
	}
	
	public void playerRemove(Player player) {
		this.playerMatches.remove(player);
		this.allPlayers.remove(player);
		return;
	}
	public ArrayList<Player> getPlayers() {
		return this.allPlayers;
	}
	
	public ArrayList<Match> getMatches() {
		return this.runningMatches;
	}
	
	public void addPlayer(Player player) {
		this.allPlayers.add(player);
	}
	
	public void replacePlayers(ArrayList<Player> players) {
		this.allPlayers.clear();
		this.allPlayers.addAll(players);
	}
	
	public void removePlayer(Player player) {
		this.allPlayers.remove(player);
	}
	
	public void addMatch(Match match) {
		this.runningMatches.add(match);
	}
	
	public void removeMatch(Match match) {
		this.runningMatches.remove(match);
	}

	public HashMap<Player, Match> getPlayerMatches() {
		return playerMatches;
	}

	public void setPlayerMatches(HashMap<Player, Match> playerMatches) {
		this.playerMatches = playerMatches;
	}
	
	public void addPlayerMatch(Player player, Match match) {
		this.playerMatches.put(player, match);
	}
	
	public void removePlayerMatch(Player player) {
		this.playerMatches.remove(player);
	}
	public HashMap<String, Match> getStringMatches() {
		return stringMatches;
	}
	public void setStringMatches(HashMap<String, Match> stringMatches) {
		this.stringMatches = stringMatches;
	}

}
