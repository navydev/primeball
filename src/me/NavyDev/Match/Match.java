package me.NavyDev.Match;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import me.NavyDev.Ball.Ball;
import me.NavyDev.MultiVersion.GetClientVersionUtil;
import me.NavyDev.MultiVersion.Version;
import me.NavyDev.PlayerStats.PlayerDrawn;
import me.NavyDev.PlayerStats.PlayerGoalScored;
import me.NavyDev.PlayerStats.PlayerLoss;
import me.NavyDev.PlayerStats.PlayerOwnGoal;
import me.NavyDev.PlayerStats.PlayerWon;
import me.NavyDev.PrimeBallMain.Utils.GetBoosterUtil;
import me.NavyDev.PrimeBallMain.Utils.LocationFromStringUtil;
import me.NavyDev.PrimeBallMain.Utils.RestoreInventoryUtil;
import me.NavyDev.PrimeBallMain.Utils.SaveInventoryUtil;
import me.NavyDev.PrimeBallMain.Utils.SendColourMessageUtil;
import me.NavyDev.Scoreboards.InGameScoreboard;
import me.NavyDev.Scoreboards.WaitingScoreboard;
import me.NavyDev.Specials.Agent;
import me.NavyDev.Specials.Commentator;
import me.navy12333.PrimeBall.PrimeBallMain;
import me.navy12333.PrimeBallMain.listeners.PrimeBallArena;
public class Match {

	private String matchName; //SET
	private HashMap<UUID, teams> playerTeams = new HashMap<UUID, teams>(); //SET
	private List<Ball> ballsInUse = new ArrayList<Ball>();; // SET
	private List<Item> ballsAsItem = new ArrayList<Item>(); //SET
	private HashMap<Item, Ball> ballItem = new HashMap<Item, Ball>(); //SET
	private ArrayList<Player> allPlayers = new ArrayList<Player>(); //SET
	private ArrayList<Player> teamOne = new ArrayList<Player>(); //SET
	private ArrayList<Player> teamTwo = new ArrayList<Player>(); //SET
	private Location ballSpawn; //SET
	private Location teamOneSpawn; //SET
	private Location teamTwoSpawn; //SET
	private Ball.physicsType physicsType;
	private int timeLeft; //SET
	private int timeUntilStart; //SET
	private int timeUntilMessage; //SET
	private int messageTimeIntervals; //SET
	private int teamOneGoals, teamTwoGoals; //SET
	private stages stage; //SET
	private MatchHandler MH; //SET
	private HashMap<Player, BoostTypes> playerBoosts = new HashMap<Player, BoostTypes>(); //SET
	private String lastGoalScorer; //SET
	private HashMap<UUID, Integer> playerGoals = new HashMap<UUID, Integer>();
	private HashMap<UUID, Integer> playerOwnGoals = new HashMap<UUID, Integer>();
	private int failedAttempts;
	private Scoreboard sb;
	private Team teamOneS;
	private Team teamTwoS;
	private HashMap<Player, Scoreboard> pSB = new HashMap<Player, Scoreboard>();
	
	public enum teams {
		TEAM_ONE,
		TEAM_TWO;
	}
	
	public enum stages {
		WAITING,
		ACTIVE,
		FINISHED;
	}
	
	public Match(String matchName, MatchHandler MH) {
		super();
		this.matchName = matchName;
		this.MH = MH;
		this.timeUntilStart = 60;
		this.stage = stages.WAITING;
		setInfo();
		MH.newMatch(this.matchName, this);
	    this.sb = Bukkit.getScoreboardManager().getNewScoreboard();
	    sb.registerNewTeam("RED");
	    teamOneS = sb.getTeam("RED");
	    teamOneS.setPrefix(ChatColor.RED + "");
	    sb.registerNewTeam("BLUE");
	    teamTwoS = sb.getTeam("BLUE");
	    teamTwoS.setPrefix(ChatColor.AQUA + "");
		return;
	}
	
	public ItemStack teamOneB() {
		return null;
		//TODO
	}
	
	public ItemStack teamTwoB() {
		return null;
		//TODO 
	}
	
	public void setInfo() {
		File arenaFile = getArenaFile();
		FileConfiguration arenaConfig = YamlConfiguration.loadConfiguration(arenaFile);
		this.ballSpawn = new LocationFromStringUtil().execute(arenaConfig.getString("ArenaData.Spawns.ballSpawn"));
		this.teamOneSpawn = new LocationFromStringUtil().execute(arenaConfig.getString("ArenaData.Spawns.teamOneSpawn"));
		this.teamTwoSpawn = new LocationFromStringUtil().execute(arenaConfig.getString("ArenaData.Spawns.teamTwoSpawn"));
		
		this.physicsType = decidePhysics(arenaConfig);
	}
	
	public Ball.physicsType decidePhysics(FileConfiguration arenaConfig) {
		String physicsAsString = arenaConfig.getString("ArenaData.Data.physicsType");
		if (physicsAsString != null) {
			if (physicsAsString.equals("classic")) {
				return Ball.physicsType.classic;
			} else if (physicsAsString.equals("navySpecial")) {
				return Ball.physicsType.navySpecial;
			} else {
				return Ball.physicsType.classic;
			} 
		} else {
			return Ball.physicsType.classic;
		} 
	}
	public void tick() {
		if (!this.getStage().equals(stages.ACTIVE)) {
			doWaitingTimer();
			
		} else {
			doMatchTimer();
			doMessageTimer();
		}
		updateScoreboards();
	}
	
	public void updateScoreboards() {
		ArrayList<Player> players = new ArrayList<Player>();
		if (this.allPlayers != null) {
			players.addAll(this.allPlayers);
			if (this.stage.equals(stages.ACTIVE)) {
				for (Player player : players) {
					new InGameScoreboard(sb, player, this.teamOneGoals, this.teamTwoGoals, this.timeLeft, this.lastGoalScorer);
				}
			} else {
				for (Player player : players) {
					new WaitingScoreboard(sb, player, this.matchName, this.timeUntilStart);
				}
			}
		}
	}
	
	public void playerLeave(Player player) {
		this.allPlayers.remove(player);
		MH.playerRemove(player);
		if (this.teamOneS.hasEntry(player.getName())) this.teamOneS.removeEntry(player.getName());
		if (this.teamOne.contains(player)) this.teamOne.remove(player);
		if (this.teamTwoS.hasEntry(player.getName())) this.teamTwoS.removeEntry(player.getName());
		if (this.teamTwo.contains(player)) this.teamTwo.remove(player);
		if (player.isOnline()) {
			Location loc = new LocationFromStringUtil().execute(PrimeBallMain.plugin.getConfig().getString("hubLocation"));
			if (loc != null) {
				player.teleport(loc);	
			} else {
				Bukkit.getLogger().log(Level.SEVERE, "PLEASE SET THE HUB FOR PRIMEBALL USING /FB HUB");
				for (OfflinePlayer offlinePlayer : Bukkit.getOperators()) {
					if (offlinePlayer.isOnline()) {
						Player op = Bukkit.getPlayer(offlinePlayer.getUniqueId());
						new SendColourMessageUtil(op, "&cThe hub for PrimeBall has not been set. Please do this by doing /fb hub.");
						new SendColourMessageUtil(op, "&cPlayers in the arena " + this.matchName + " are stranded.");
					}
				}
			}
			new RestoreInventoryUtil(player);
			player.setScoreboard(pSB.get(player));
			pSB.remove(player);
		} else {
			PrimeBallMain.earlyLeavers.add(player);
		}
		if (this.allPlayers.size() == 0) {
			if (!this.stage.equals(stages.FINISHED)) {
				onEnd();	
			}
		}
	}
	public void playerJoin(Player player, teams team) {
		this.allPlayers.add(player);
		MH.addPlayer(player);
		MH.addPlayerMatch(player, this);
		new SaveInventoryUtil(player);
		player.getInventory().clear();
		player.getInventory().setBoots(null);
		player.getInventory().setChestplate(null);
		player.getInventory().setHelmet(null);
		player.getInventory().setLeggings(null);
		player.updateInventory();
		pSB.put(player, player.getScoreboard());
		if (team != null) {
			if (team.equals(teams.TEAM_ONE)) playerSetTeamOne(player);
			else if (team.equals(teams.TEAM_TWO)) playerSetTeamTwo(player);
		} else {
			decideTeam(player, this.allPlayers.size());
		}
		new WaitingScoreboard(sb, player, this.matchName, this.timeUntilStart);
	}
	
	public void decideTeam(Player player, int iteration) {
		if (!this.stage.equals(stages.ACTIVE)) {
			if (iteration % 2 == 0) {
				playerSetTeamOne(player);
			} else {
				playerSetTeamTwo(player);
			}
		} else {
			if (this.teamOne.size() > this.teamTwo.size()) {
				playerSetTeamTwo(player);
			} else if (this.teamOne.size() < this.teamTwo.size()) {
				playerSetTeamOne(player);
			} else {
				if (iteration % 2 == 0) {
					playerSetTeamOne(player);
				} else {
					playerSetTeamTwo(player);
				}
			}
		}
	}
	
	public void playerSetTeamOne(Player player) {
		this.playerTeams.put(player.getUniqueId(), teams.TEAM_ONE);
		this.teamOne.add(player);
		String title = ChatColor.translateAlternateColorCodes('&', "&c&lRED");
		String subtitle = ChatColor.translateAlternateColorCodes('&', "&7This is the team you're on");
		Version serverVersion = new GetClientVersionUtil().execute();
		if (serverVersion.equals(Version.ONE_ELEVEN) || serverVersion.equals(Version.ONE_TWELVE)) player.sendTitle(title, subtitle, 10, 20, 10);
		else player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&0[&aPrimeBall&0] &cYou're on team red."));
		if (PrimeBallMain.plugin.getConfig().getBoolean("useInGameHelmets"))
				player.getInventory().setHelmet(new ItemStack(Material.STAINED_GLASS, 1, (short) 14));
		this.teamOneS.addEntry(player.getName());
		player.setScoreboard(this.sb);
	}
	
	public void playerSetTeamTwo(Player player) {
		this.playerTeams.put(player.getUniqueId(), teams.TEAM_TWO);
		this.teamTwo.add(player);
		String title = ChatColor.translateAlternateColorCodes('&', "&b&lBLUE");
		String subtitle = ChatColor.translateAlternateColorCodes('&', "&7This is the team you're on");
		Version serverVersion = new GetClientVersionUtil().execute();
		if (serverVersion.equals(Version.ONE_ELEVEN) || serverVersion.equals(Version.ONE_TWELVE)) player.sendTitle(title, subtitle, 10, 20, 10);
		else player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&0[&aPrimeBall&0] &bYou're on team blue."));
		if (PrimeBallMain.plugin.getConfig().getBoolean("useInGameHelmets"))
			player.getInventory().setHelmet(new ItemStack(Material.STAINED_GLASS, 1, (short) 3));
		this.teamTwoS.addEntry(player.getName());
		player.setScoreboard(this.sb);
	}
	
	public void playerLateJoin(Player player, teams team) {
		this.allPlayers.add(player);
		MH.addPlayer(player);
		MH.addPlayerMatch(player, this);
		new SaveInventoryUtil(player);
		player.getInventory().clear();
		player.getInventory().setBoots(null);
		player.getInventory().setChestplate(null);
		player.getInventory().setHelmet(null);
		player.getInventory().setLeggings(null);
		player.updateInventory();
		pSB.put(player, player.getScoreboard());
		if (team != null) {
			if (team.equals(teams.TEAM_ONE)) playerSetTeamOne(player);
			else if (team.equals(teams.TEAM_TWO)) playerSetTeamTwo(player);
		} else {
			decideTeam(player, this.allPlayers.size());
		}
		new InGameScoreboard(sb, player, this.teamOneGoals, this.teamTwoGoals, this.timeLeft, this.lastGoalScorer);
		if (this.playerTeams.get(player.getUniqueId()).equals(teams.TEAM_ONE)) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(PrimeBallMain.plugin, new Runnable() {
				@Override
				public void run() {
					player.teleport(teamOneSpawn);
				}
			}, 60L);
		}
		else if (this.playerTeams.get(player.getUniqueId()).equals(teams.TEAM_TWO)){
			Bukkit.getScheduler().scheduleSyncDelayedTask(PrimeBallMain.plugin, new Runnable() {
				@Override
				public void run() {
					player.teleport(teamTwoSpawn);
				}
			}, 60L);
							
		}
	}
	
	public void onOpen() {
		ArrayList<Player> players = this.allPlayers;
		for (Player arenaPlayer : players) {
			checkForBoost(arenaPlayer);
		}
		tpTeams();
		doTeamBreakDowns(players);
		for (Player player : players) {
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Your match will begin soon."));
		}
		return;
	}
	
	public void onStart() {
		if (this.allPlayers.size() >= 2 && teamsKindaBalanced()) {
			this.stage = stages.ACTIVE;
			Location location = ballSpawn;
			spawnBall(location);
			tpTeams();
			File arenaFile = getArenaFile();
			FileConfiguration arenaConfig = YamlConfiguration.loadConfiguration(arenaFile);
			this.timeLeft = arenaConfig.getInt("ArenaData.Data.runTime");
			this.messageTimeIntervals = this.timeLeft / 3;
			this.timeUntilMessage = this.messageTimeIntervals;
			ArrayList<Player> players = new ArrayList<Player>();
			players.addAll(this.allPlayers);
			for (Player player : players) {
				player.setGameMode(GameMode.SURVIVAL);
				new InGameScoreboard(sb, player, this.teamOneGoals, this.teamTwoGoals, this.timeLeft, this.lastGoalScorer);
			}
		} else {
			ArrayList<Player> players = this.allPlayers;
			matchMessages("&cYour arena has been cancelled as there was not enough players to start.");
			for (Player player : players) {
				playerLeave(player);
			}
			MH.deleteMatch(this.matchName, this);
		}
	}
	
	public void onEnd() {
		this.stage = stages.FINISHED;
		removeBalls();
		String winningTeam = workOutWinningTeam();
		ArrayList<Player> players = new ArrayList<Player>();
		if (!this.allPlayers.isEmpty()) {
			players.addAll(this.allPlayers);
			for (Player player : players) {
				if (playerBoosts.containsKey(player)) {
					BoostTypes boost = playerBoosts.get(player);
					if (boost.equals(BoostTypes.FIVE_BOOST)) {
						new Boosts().fiveBoost(player);
					} else if (boost.equals(BoostTypes.TEN_BOOST)) {
						new Boosts().tenBoost(player);
					} else if (boost.equals(BoostTypes.TWENTY_FIVE_BOOST)) {
						new Boosts().twentyFiveBoost(player);
					} else if (boost.equals(BoostTypes.FIFTY_BOOST)) {
						new Boosts().fiftyBoost(player);
					}
				}	
			}
			
			if (winningTeam.equalsIgnoreCase("draw")) {
				for (Player player : players) {
					doDrawReward(player);
					if (!casualArena()) {
						new PlayerDrawn(player);
					}
				}
			} else if (winningTeam.equalsIgnoreCase("teamOne")) {
				for (Player player : this.teamOne) {
					doWinReward(player);
					if (!casualArena()) {
						new PlayerWon(player);
					}
				}
				
				for (Player player : this.teamTwo) {
					doLoseReward(player);
					if (!casualArena()) {
						new PlayerLoss(player);
					}
				}
			} else {
				for (Player player : this.teamTwo) {
					doWinReward(player);
					if (!casualArena()) {
						new PlayerWon(player);
					}
				}
				
				for (Player player : this.teamOne) {
					doLoseReward(player);
					if (!casualArena()) {
						new PlayerLoss(player);
					}
				}
			}
			String goalScorers = "";
			HashMap<UUID, Integer> goals = new HashMap<UUID, Integer>();
			goals.putAll(this.playerGoals);
			for (UUID ID : goals.keySet()) {
				Player player = Bukkit.getPlayer(ID);
				if (player != null && player.isOnline())
				goalScorers = goalScorers + "&7" + player.getName() + ": " + goals.get(ID) + "&6, ";
			}
			matchMessages("&9&m----------&7 Goals &9&m----------");
			matchMessages(goalScorers);
			goals.clear();
			goals.putAll(this.playerOwnGoals);
			goalScorers = "";
			for (UUID ID : goals.keySet()) {
				Player player = Bukkit.getPlayer(ID);
				if (player != null && player.isOnline())
				goalScorers = goalScorers + "&7" + player.getName() + ": " + goals.get(ID) + "&6, ";
			}
			matchMessages("&9&m--------&c Own Goals &9&m--------");
			matchMessages(goalScorers);
			for (Player player : players) {
				playerLeave(player);
			}
			MH.deleteMatch(this.matchName, this);	
			this.allPlayers.clear();
		}
	}
	
	public void doWinReward(Player player) {
		FileConfiguration config = PrimeBallMain.plugin.getConfig();
		ConsoleCommandSender console = Bukkit.getConsoleSender();
		if (config.getStringList("Rewards.onWin") != null) {
			List<String> rewards = config.getStringList("Rewards.onWin");
			if (!rewards.isEmpty()) {
				for (String command : rewards) {
					command = command.replace("{name}", player.getName());
					Bukkit.dispatchCommand(console, command);
				}
			}
		}
	}
	
	public void doDrawReward(Player player) {
		FileConfiguration config = PrimeBallMain.plugin.getConfig();
		ConsoleCommandSender console = Bukkit.getConsoleSender();
		if (config.getStringList("Rewards.onDraw") != null) {
			List<String> rewards = config.getStringList("Rewards.onDraw");
			if (!rewards.isEmpty()) {
				for (String command : rewards) {
					command = command.replace("{name}", player.getName());
					Bukkit.dispatchCommand(console, command);
				}
			}
		}
	}
	
	public void doLoseReward(Player player) {
		FileConfiguration config = PrimeBallMain.plugin.getConfig();
		ConsoleCommandSender console = Bukkit.getConsoleSender();
		if (config.getStringList("Rewards.onLose") != null) {
			List<String> rewards = config.getStringList("Rewards.onLose");
			if (!rewards.isEmpty()) {
				for (String command : rewards) {
					command = command.replace("{name}", player.getName());
					Bukkit.dispatchCommand(console, command);
				}
			}
		}
	}
	
	public void onGoalScored(Material material, Ball ball) {
		Item item = ball.getItem();
	    if (material.equals(Material.STONE)) {
	    	this.teamOneGoals = this.teamOneGoals + 1;
	    	new PrimeBallArena().createFireWork(this.ballSpawn, Color.RED, this.teamOneGoals);
	    }
	    if (material.equals(Material.COBBLESTONE)){
	    	this.teamTwoGoals = this.teamTwoGoals + 1;
	    	new PrimeBallArena().createFireWork(this.ballSpawn, Color.BLUE, this.teamTwoGoals);
	    }	     

	    Location OgLoc = ball.getLastKickedFrom();
	    Location BallLoc = item.getLocation();

	    double nDistance = OgLoc.distance(BallLoc);
	    int distance = (int) round(nDistance,1);
	      
	    
	    item.remove();
	    spawnBall(this.ballSpawn);
	    tpTeams();
	    
		if (material.equals(Material.STONE)) {
			int ScoringPlayersTeam = 1;
			if (teamOne.contains(ball.getLastKickedBy())) {
				ScoringPlayersTeam = 0;
			} else {
				ScoringPlayersTeam = 1;
			}
				if (ScoringPlayersTeam == 1) {
					matchMessages("&0[&bCommentator&0] &6Oh and &b" + ball.getLastKickedBy().getName() +
							" &6has scored an own goal! Giving team &cRED &6a goal!");
					this.lastGoalScorer = ball.getLastKickedBy().getName() + " (OG)";
					if (!casualArena()) new PlayerOwnGoal(ball.getLastKickedBy());
					addInternalOwnGoal(ball.getLastKickedBy());
				} else {
					this.lastGoalScorer = ball.getLastKickedBy().getName();
					addInternalGoal(ball.getLastKickedBy());
					if (!casualArena()) new PlayerGoalScored(ball.getLastKickedBy());
					if (!casualArena()) new Agent(ball.getLastKickedBy());
					String message = new Commentator().execute(ball.getLastKickedBy(), distance, this);
					matchMessages(message);
			}
		}
		if (material.equals(Material.COBBLESTONE)) {
			int ScoringPlayersTeam = 1;
			if (teamTwo.contains(ball.getLastKickedBy())) {
				ScoringPlayersTeam = 1;
			} else {
				ScoringPlayersTeam = 0;
			}
			if (ScoringPlayersTeam == 0) {
				matchMessages("&0[&bCommentator&0] &6Oh and &b" + ball.getLastKickedBy().getName() +
						" &6has scored an own goal! Giving team &bBLUE &6a goal!");
				this.lastGoalScorer = ball.getLastKickedBy().getName() + " (OG)";
				addInternalOwnGoal(ball.getLastKickedBy());
				if (!casualArena()) new PlayerOwnGoal(ball.getLastKickedBy());
			} else {
				this.lastGoalScorer = ball.getLastKickedBy().getName();
				addInternalGoal(ball.getLastKickedBy());
				if (!casualArena()) new PlayerGoalScored(ball.getLastKickedBy());
				if (!casualArena()) new Agent(ball.getLastKickedBy());
				String message = new Commentator().execute(ball.getLastKickedBy(), distance, this);
				matchMessages(message);
			}
		}
	}
	
	public void addInternalOwnGoal(Player player) {
		UUID ID = player.getUniqueId();
		if (this.playerOwnGoals.containsKey(ID)) {
			int ownGoals = Integer.parseInt(this.playerOwnGoals.get(ID).toString());
			ownGoals = ownGoals + 1;
			this.playerOwnGoals.remove(ID);
			this.playerOwnGoals.put(ID, ownGoals);
		} else {
			this.playerOwnGoals.put(ID, 1);
		}
	}
	
	public void addInternalGoal(Player player) {
		UUID ID = player.getUniqueId();
		if (this.playerGoals.containsKey(ID)) {
			int goals = Integer.parseInt(this.playerGoals.get(ID).toString());
			goals = goals + 1;
			this.playerGoals.remove(ID);
			this.playerGoals.put(ID, goals);
		} else {
			this.playerGoals.put(ID, 1);
		}
	}
	
	public void doWaitingTimer() {
		if (this.allPlayers.size() > 0) {
			int newTime = this.timeUntilStart;
			if (newTime > 0) {
				if (newTime != 10) {
					newTime = newTime - 1;
					this.timeUntilStart = newTime;
					return;
				} else {
					newTime = newTime - 1;
					this.timeUntilStart = newTime;
					checkStart();
					return;
				}
			} else {
				onStart();
				return;
			}
		} else {
			MH.deleteMatch(this.matchName, this);
		}
	}
	
	public void forceStart() {
		this.timeUntilStart = 9;
		onOpen();
	}
	
	public void checkStart() {
		int size = this.allPlayers.size();
		if (size < 2 && !teamsKindaBalanced()) {
			if (this.failedAttempts < 2) {
				this.timeUntilStart = this.timeUntilStart + 20;
				this.failedAttempts = this.failedAttempts + 1;
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&0[&6PrimeBall&0] "
						+ "&aThe arena &7" + this.matchName + " &arequires more players to start!"));
				Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&0[&6PrimeBall&0] "
						+ "&aUse &7/fb join " + this.matchName + " &ato join."));
			} else {
				ArrayList<Player> players = this.allPlayers;
				matchMessages("&cYour arena has been cancelled as there was not enough players to start.");
				for (Player player : players) {
					playerLeave(player);
					MH.deleteMatch(this.matchName, this);
				}
			}
		} else {
			onOpen();
		}
		return;
	}
	
	public boolean teamsKindaBalanced() {
		if (this.teamOne.size() == this.teamTwo.size()) return true;
		if (this.teamOne.size() -1 == this.teamTwo.size()) return true;
		if (this.teamOne.size() == this.teamTwo.size() -1) return true;
		return false;
	}
	public void doMatchTimer() {
		if (this.allPlayers != null) {
			if (this.allPlayers.size() == 0) {
				MH.deleteMatch(this.matchName, this);
				return;
			}
			int newTime = this.timeLeft;
			if (newTime > 0) {
				newTime = newTime - 1;
				this.timeLeft = newTime;
				return;
			} else {
				if (!this.stage.equals(stages.FINISHED)) {
					onEnd();	
				}
			}
		}
	}
	
	public void doMessageTimer() {
		int newTime = this.timeUntilMessage;
		if (newTime > 0) {
			newTime = newTime - 1;
			this.timeUntilMessage = newTime;
			return;
		} else {
			this.timeUntilMessage = this.messageTimeIntervals;
			String winningTeam = workOutWinningTeam();
			String losingTeam = workOutLosingTeam();
			winningTeam = winningTeam.replace("teamOne", "&cRED").replace("teamTwo", "&bBLUE");
			losingTeam = losingTeam.replace("teamOne", "&cRED").replace("teamTwo", "&bBLUE");
			if (!winningTeam.equals("draw")) {
				matchMessages("&0[&cCommentator&0] &6With &7" + this.timeLeft + " seconds &6remaining &6and " + winningTeam + 
						"&6 in the lead " + losingTeam + "&6 will look to make a comeback!");
			} else {
				matchMessages("&0[&cCommentator&0] &6Only one goal needed to take the lead! Lets see who can get it before the game"
						+ " ends in &7" + this.timeLeft + " seconds.");
			}
			return;
		}
	}
	
	public String workOutWinningTeam() {
		int teamOneG = this.teamOneGoals;
		int teamTwoG = this.teamTwoGoals;
		if (teamOneG == teamTwoG) {
			return "draw";
		}
		if (teamOneG > teamTwoG) {
			return "teamOne";
		} else {
			return "teamTwo";
		}
	}
	
	public String workOutLosingTeam() {
		int teamOneG = this.teamOneGoals;
		int teamTwoG = this.teamTwoGoals;
		if (teamOneG == teamTwoG) {
			return "draw";
		}
		if (teamOneG > teamTwoG) {
			return "teamTwo";
		} else {
			return "teamOne";
		}
	}
	public void removeBalls() {
		if (this.ballsInUse != null) {
			List<Ball> balls = new ArrayList<Ball>();
			if (!this.ballsInUse.isEmpty()) {
				balls.addAll(this.ballsInUse);
				for (Ball ball : balls) {
					Location loc = ball.getItem().getLocation();
					World world = loc.getWorld();
					if (!world.getChunkAt(loc).isLoaded()) {
						world.getChunkAt(loc).load();
					}
					ball.getItem().remove();
					world.getChunkAt(loc).unload();
				}
			}
		}
	}
	
	public void spawnBall(Location loc) {
		loc.setY(loc.getY() + 1);
		Ball newBall = new Ball().spawnBall(loc);
		this.ballsInUse.add(newBall);
		return;
	}
	
	public void doTeamBreakDowns(ArrayList<Player> players) {
		double redTPV = 0.0;
		int redGs = 0, redOgs = 0, redWs = 0;
		
		double blueTPV = 0.0;
		int blueGs = 0, blueOgs = 0, blueWs = 0;
		
		int normUse = 0;
		double douUse = 0.0;
	
		for (Player player : players) {
			PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		 	File NewCard = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder(), File.separator + "/PlayerStats/");
			String playerUU = player.getUniqueId().toString();
			File f = new File(NewCard, File.separator + playerUU + ".yml");
			FileConfiguration Team = YamlConfiguration.loadConfiguration(f);
			 
			if (this.teamOne.contains(player)) {
				normUse = Team.getInt("Player." + playerUU + ".GS" );
				redGs = redGs + normUse;
				normUse = Team.getInt("Player." + playerUU + ".OG" );
				redOgs = redOgs + normUse;
				normUse = Team.getInt("Player." + playerUU + ".MW" );
				redWs = redWs + normUse;
				douUse = Team.getDouble("Player." + playerUU + ".PV" );
				redTPV = redTPV + douUse;
			} else if (this.teamTwo.contains(player)) {
				normUse = Team.getInt("Player." + playerUU + ".GS" );
				blueGs = blueGs + normUse;
				normUse = Team.getInt("Player." + playerUU + ".OG" );
				blueOgs = blueOgs + normUse;
				normUse = Team.getInt("Player." + playerUU + ".MW" );
				blueWs = blueWs + normUse;
				douUse = Team.getDouble("Player." + playerUU + ".PV" );
				blueTPV = blueTPV + douUse;
			}
			redTPV = round(redTPV, 2);
			blueTPV = round(blueTPV, 2);
			
		}
		for (Player arenaPlayer : players) {
			arenaPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6-- Team Break Downs --"));
			arenaPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&bTeam &4Red:"));
			arenaPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&bTotal Goals: &4" + redGs));
			arenaPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&bTotal Own Goals: &4" + redOgs));
			arenaPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&bTotal Wins: &4" + redWs));
			arenaPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Team Value: &4" + redTPV));
			arenaPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', " "));
			arenaPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&bTeam &5Blue:"));
			arenaPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&bTotal Goals: &5" + blueGs));
			arenaPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&bTotal Own Goals: &5" + blueOgs));
			arenaPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&bTotal Wins: &5" + blueWs));
			arenaPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Team Value: &5" + blueTPV));
			arenaPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6-- End Of Team Break Downs --"));
			arenaPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', " "));
		} 
	}
	
	public void checkForBoost(Player player) {
		PlayerInventory inv = player.getInventory();

		for (int a = 0 ; a < inv.getSize() ; a++ ){
            ItemStack is = inv.getItem(a);
            if (is != null) {
	            	
	            ItemMeta itemMeta = is.getItemMeta();
	            String item = itemMeta.getDisplayName();
	            int Amount = is.getAmount();
	            int slot = a;
	            if (item != null) {
		            if (item.contains("5% Booster")) {
		        		ArrayList<String> lore = new ArrayList<String>();
		        		lore.add(ChatColor.BOLD + "5% PV and Cash Boost!");
						
						if (Amount == 1) {
							inv.setItem(slot, null);
							this.playerBoosts.put(player, BoostTypes.FIVE_BOOST);
							player.sendMessage(ChatColor.LIGHT_PURPLE + "You're using a 5% PV and cash Booster!");
							break;
						}
						else {
							ItemStack boost = new GetBoosterUtil().execute("&b&l5% Booster", Material.STAINED_CLAY, Amount - 1, (short) 11, lore);
							inv.setItem(slot, boost);
							this.playerBoosts.put(player, BoostTypes.FIVE_BOOST);
							player.sendMessage(ChatColor.LIGHT_PURPLE + "You're using a 5% PV and cash Booster!");
							break;
						}
		            }
					else if (item.contains("10% Booster")){
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(ChatColor.BOLD + "10% PV and Cash Boost!");
						if (Amount == 1) {
							inv.setItem(slot, null);
							player.sendMessage(ChatColor.LIGHT_PURPLE + "You're using a 10% PV and cash Booster!");
							break;
						}
						else {
							ItemStack boost = new GetBoosterUtil().execute("&b&l10% Booster", Material.STAINED_CLAY, Amount - 1, (short) 4, lore);
							inv.setItem(slot, boost);
							player.sendMessage(ChatColor.LIGHT_PURPLE + "You're using a 10% PV and cash Booster!");
							break;
						}
					}
					else if (item.contains("25% Booster")){
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(ChatColor.BOLD + "25% PV and Cash Boost!");
						if (Amount == 1) {
							inv.setItem(slot, null);
							player.sendMessage(ChatColor.LIGHT_PURPLE + "You're using a 25% PV and cash Booster!");
							break;
						}
						else {
							ItemStack boost = new GetBoosterUtil().execute("&b&l25% Booster", Material.STAINED_CLAY, Amount - 1, (short) 1, lore);
							inv.setItem(slot, boost);
							player.sendMessage(ChatColor.LIGHT_PURPLE + "You're using a 25% PV and cash Booster!");
							break;
						}
					}
					else if (item.contains("50% Booster")){
						ArrayList<String> lore = new ArrayList<String>();
						lore.add(ChatColor.BOLD + "50% PV and Cash Boost!");
						if (Amount == 1) {
							inv.setItem(slot, null);
							player.sendMessage(ChatColor.LIGHT_PURPLE + "You're using a 50% PV and cash Booster!");
							break;
						}
						else {
							ItemStack boost = new GetBoosterUtil().execute("&b&l25% Booster", Material.STAINED_CLAY, Amount - 1, (short) 14, lore);
							inv.setItem(slot , boost);
							player.sendMessage(ChatColor.LIGHT_PURPLE + "You're using a 50% PV and cash Booster!");
							break;
						}
					}        
	            }
            }
		}	
	}
	
	public double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public void matchMessages(String message) {
		ArrayList<Player> players = this.allPlayers;
		for (Player player : players) {
			if (player.isOnline()) {
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
			}
		}
	}

	public void tpTeams() {
		ArrayList<Player> setOne = this.teamOne;
		ArrayList<Player> setTwo = this.teamTwo;
		for (Player player : setOne) {
			if (player.isOnline()) {
				player.teleport(teamOneSpawn);
			}
		}
		for (Player player : setTwo) {
			if (player.isOnline()) {
				player.teleport(teamTwoSpawn);
			}
		}
		return;
	}
	
	
	public void checkBall() {
		if (this.ballsInUse != null) {
			if (!this.ballsInUse.isEmpty()) {
				List<Ball> balls = new ArrayList<Ball>();
				balls.addAll(this.ballsInUse);
				for (Ball ball : balls) {
					Item item = ball.getItem();
					if (!item.isDead()) {
						Location loc = item.getLocation();
							      
					      Block blockD = loc.getBlock().getRelative(BlockFace.DOWN);
					      Block blockE = loc.getBlock().getRelative(BlockFace.EAST);
					      Block blockW = loc.getBlock().getRelative(BlockFace.WEST);
					      Block blockS = loc.getBlock().getRelative(BlockFace.SOUTH);
					      Block blockN = loc.getBlock().getRelative(BlockFace.NORTH);
					      
					      Material materialD = blockD.getType();
					      Material materialE = blockE.getType();
					      Material materialW = blockW.getType();
					      Material materialS = blockS.getType();
					      Material materialN = blockN.getType();
					      
					      if (materialD.equals(Material.STONE) || materialE.equals(Material.STONE) || materialW.equals(Material.STONE) 
					    		  || materialS.equals(Material.STONE)|| materialN.equals(Material.STONE))
					      {
					    	  Material material = Material.STONE;
					    	  onGoalScored(material, ball);
					      }
					      if (materialD.equals(Material.COBBLESTONE) || materialE.equals(Material.COBBLESTONE) || materialW.equals(Material.COBBLESTONE) 
					    		  || materialS.equals(Material.COBBLESTONE) || materialN.equals(Material.COBBLESTONE))
					      {
					    	  Material material = Material.COBBLESTONE;
					    	  onGoalScored(material, ball);
					      }
					} else {
						this.ballsInUse.remove(ball);
						this.ballsAsItem.remove(item);
						this.ballItem.remove(item);
					}
				}
			}
		}
	}

	
	public File getArenaFile() {
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/Arenas/");
		File f = new File(folder, File.separator + this.matchName + ".yml");

		return f;
	}
	
	public boolean casualArena() {
		String arenaName = this.getMatchName();
		if (PrimeBallMain.plugin.getConfig().getStringList("noStatsArenas") != null) {
			List<String> casualArenas = PrimeBallMain.plugin.getConfig().getStringList("noStatsArenas");
			for (String str : casualArenas) {
				if (str.equalsIgnoreCase(arenaName)) return true;
			}
		}
		return false;
	}
	
	public String getMatchName() {
		return this.matchName;
	}
	
	public List<Ball> getArenaBalls() {
		return this.ballsInUse;
	}

	public List<Item> getBallsAsItem() {
		return ballsAsItem;
	}

	public void setBallsAsItem(List<Item> ballsAsItem) {
		this.ballsAsItem = ballsAsItem;
	}

	public HashMap<Item, Ball> getBallItem() {
		return ballItem;
	}

	public void setBallItem(HashMap<Item, Ball> ballItem) {
		this.ballItem = ballItem;
	}

	public int getTimeLeft() {
		return timeLeft;
	}

	public void setTimeLeft(int timeLeft) {
		this.timeLeft = timeLeft;
	}

	public int getMessageTimeIntervals() {
		return messageTimeIntervals;
	}

	public void setMessageTimeIntervals(int messageTimeIntervals) {
		this.messageTimeIntervals = messageTimeIntervals;
	}

	public int getTimeUntilMessage() {
		return timeUntilMessage;
	}

	public void setTimeUntilMessage(int timeUntilMessage) {
		this.timeUntilMessage = timeUntilMessage;
	}

	public MatchHandler getMatchHandler() {
		return MH;
	}

	public void setMatchHandler(MatchHandler mH) {
		MH = mH;
	}

	public int getTimeUntilStart() {
		return timeUntilStart;
	}

	public void setTimeUntilStart(int timeUntilStart) {
		this.timeUntilStart = timeUntilStart;
	}

	public stages getStage() {
		return stage;
	}

	public void setStage(stages stage) {
		this.stage = stage;
	}

	public String getLastGoalScorer() {
		return lastGoalScorer;
	}

	public void setLastGoalScorer(String lastGoalScorer) {
		this.lastGoalScorer = lastGoalScorer;
	}

	public HashMap<UUID, Integer> getPlayerGoals() {
		return playerGoals;
	}

	public void setPlayerGoals(HashMap<UUID, Integer> playerGoals) {
		this.playerGoals = playerGoals;
	}

	public HashMap<UUID, Integer> getPlayerOwnGoals() {
		return playerOwnGoals;
	}

	public void setPlayerOwnGoals(HashMap<UUID, Integer> playerOwnGoals) {
		this.playerOwnGoals = playerOwnGoals;
	}

	public int getFailedAttempts() {
		return failedAttempts;
	}

	public void setFailedAttempts(int failedAttempts) {
		this.failedAttempts = failedAttempts;
	}

	public Ball.physicsType getPhysicsType() {
		return physicsType;
	}

	public void setPhysicsType(Ball.physicsType physicsType) {
		this.physicsType = physicsType;
	}
	
	public int getAmountPlaying() {
		return this.playerTeams.keySet().size();
	}
	
	public int getAmountPlayers() {
		return this.allPlayers.size();
	}
	
	public String getScore() {
		return "&c&lRED &8" + this.teamOneGoals + " &7- &8" + this.teamTwoGoals + " &b&lBLUE";
	}
}
