package me.NavyDev.Scoreboards;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import me.navy12333.PrimeBall.PrimeBallMain;

public class StatsScoreboard {

	Scoreboard stats = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
	Scoreboard pScoreboard;

	public StatsScoreboard(Player player) {
		File playerFile = getPlayerFile(player);
		FileConfiguration playerConfig = YamlConfiguration.loadConfiguration(playerFile);
		String playerUU = player.getUniqueId().toString();
		pScoreboard = player.getScoreboard();
		int MP = playerConfig.getInt("Player." + playerUU + ".MP");
		int MW = playerConfig.getInt("Player." + playerUU + ".MW");
		int ML = playerConfig.getInt("Player." + playerUU + ".ML");
		int MD = playerConfig.getInt("Player." + playerUU + ".MD");
		int GS = playerConfig.getInt("Player." + playerUU + ".GS");
		int OG = playerConfig.getInt("Player." + playerUU + ".OG");
		int PO = playerConfig.getInt("Player." + playerUU + ".PO");
		int PN = playerConfig.getInt("Player." + playerUU + ".PN");
		int LG = playerConfig.getInt("Player." + playerUU + ".LG");
		int SG = playerConfig.getInt("Player." + playerUU + ".SG");
		int NG = playerConfig.getInt("Player." + playerUU + ".NG");
		String PV = playerConfig.getString("Player." + playerUU + ".PV");

		String playerName = player.getName();

		Objective objective = stats.registerNewObjective("PWins", "PLoses");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&c&nFootball Stats"));
		Score playername1 = objective.getScore(ChatColor.translateAlternateColorCodes('&', "&cName: " + playerName));
		playername1.setScore(18);
		Score space1 = objective.getScore(" ");
		space1.setScore(17);
		Score Mp = objective.getScore(ChatColor.WHITE + "" + ChatColor.BOLD + "Matches played: " + MP);
		Mp.setScore(16);
		Score Mw = objective.getScore(ChatColor.GREEN + "" + ChatColor.BOLD + "Matches won: " + MW);
		Mw.setScore(15);
		Score Md = objective.getScore(ChatColor.GRAY + "" + ChatColor.BOLD + "Matches drawn: " + MD);
		Md.setScore(14);
		Score Ml = objective.getScore(ChatColor.RED + "" + ChatColor.BOLD + "Matches lost: " + ML);
		Ml.setScore(13);
		double Mww = MW;
		double Mpp = MP;
		double WP = ((Mww / Mpp) * 100);
		WP = Math.round(WP);
		Score Wp = objective.getScore(ChatColor.GREEN + "" + ChatColor.BOLD + "Win percentage: " + WP + "%");
		Wp.setScore(12);

		Score space2 = objective.getScore("  ");
		space2.setScore(11);

		Score Gs = objective.getScore(ChatColor.GREEN + "" + ChatColor.BOLD + "Goals scored: " + GS);
		Gs.setScore(10);
		Score Ng = objective.getScore(ChatColor.WHITE + "" + ChatColor.BOLD + "Normal goals scored: " + NG);
		Ng.setScore(9);
		Score Sg = objective.getScore(ChatColor.WHITE + "" + ChatColor.BOLD + "Short goals scored: " + SG);
		Sg.setScore(8);
		Score Lg = objective.getScore(ChatColor.WHITE + "" + ChatColor.BOLD + "Long goals scored: " + LG);
		Lg.setScore(7);
		Score Og = objective.getScore(ChatColor.RED + "" + ChatColor.BOLD + "Own goals: " + OG);
		Og.setScore(6);

		Score space3 = objective.getScore("   ");
		space3.setScore(5);

		Score Ds = objective.getScore(ChatColor.WHITE + "" + ChatColor.BOLD + "Points: " + PO);
		Ds.setScore(4);
		Score Pn = objective.getScore(ChatColor.WHITE + "" + ChatColor.BOLD + "Punches completed: " + PN);
		Pn.setScore(3);
		Score space4 = objective.getScore("    ");
		space4.setScore(2);
		Score Pv = objective.getScore(ChatColor.GREEN + "" + ChatColor.BOLD + "Player value: " + PV + " M");
		Pv.setScore(1);
		player.setScoreboard(stats);

		Bukkit.getScheduler().runTaskLater(PrimeBallMain.plugin, new Runnable() {
			@Override
			public void run() {
				if (player.getScoreboard() != null) {
					if (player.getScoreboard().equals(stats)) {
						if (pScoreboard != null) {
							player.setScoreboard(pScoreboard);
						} else {
							player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
						}
					}
				}
			}
		}, 200L);
	}

	public File getPlayerFile(Player player) {
		String playerUU = player.getUniqueId().toString();
		PluginDescriptionFile pdfFile = PrimeBallMain.plugin.getDescription();
		File folder = new File(
				Bukkit.getServer().getPluginManager().getPlugin(pdfFile.getName()).getDataFolder() + "/PlayerStats/");
		File f = new File(folder, File.separator + playerUU + ".yml");

		return f;
	}
}
