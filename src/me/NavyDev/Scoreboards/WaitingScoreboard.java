package me.NavyDev.Scoreboards;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class WaitingScoreboard {

	
	public WaitingScoreboard(Scoreboard stats, Player player, String arena, int waitingFor) {
		if (player.isOnline()) {
			player.setFoodLevel(20);
			if (stats.getObjective(DisplaySlot.SIDEBAR) == null) {
				Objective objective = stats.registerNewObjective("PWins", "PLoses");	
				objective.setDisplaySlot(DisplaySlot.SIDEBAR);
				objective.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&c&l&nPrimeBall"));
				
				Score space1 = objective.getScore(" ");
				space1.setScore(15);
				
				Team mp = stats.registerNewTeam("mp");
				mp.addEntry(ChatColor.BLACK + "");
				mp.setPrefix(ChatColor.translateAlternateColorCodes('&', "&7Waiting."));
				objective.getScore(ChatColor.BLACK + "").setScore(14);
				
				Score space2 = objective.getScore("   ");
				space2.setScore(13);
				
				Team mm = stats.registerNewTeam("mm");
				mm.addEntry(ChatColor.DARK_AQUA + "");
				mm.setPrefix(ChatColor.translateAlternateColorCodes('&', "&6Arena: "));
				objective.getScore(ChatColor.DARK_AQUA + "").setScore(12);
				
				Team mz= stats.registerNewTeam("mz");
				mz.addEntry(ChatColor.DARK_BLUE + "");
				mz.setPrefix(ChatColor.translateAlternateColorCodes('&', "&6"+ arena.toUpperCase()));
				objective.getScore(ChatColor.DARK_BLUE + "").setScore(11);
				
				Team mk = stats.registerNewTeam("mk");
				mk.addEntry(ChatColor.DARK_GREEN + "");
				mk.setPrefix(ChatColor.translateAlternateColorCodes('&',"&a" + waitingFor + " (s)" ));
				objective.getScore(ChatColor.DARK_GREEN + "").setScore(10);
				
				player.setScoreboard(stats);
			} else {
				stats.getTeam("mz").setPrefix(ChatColor.translateAlternateColorCodes('&', "&6"+ arena.toUpperCase()));
				stats.getTeam("mk").setPrefix(ChatColor.translateAlternateColorCodes('&', "&a" + waitingFor + " seconds"));
			}
		}
	}
}
