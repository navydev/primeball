package me.NavyDev.Scoreboards;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class InGameScoreboard {

	public InGameScoreboard(Scoreboard stats, Player player, int teamOneGoals, int teamTwoGoals, int timeLeft, String lastGoalScorer) {
		if (player.isOnline()) {
			if (stats.getTeam("km") == null) {
				Objective objective = stats.registerNewObjective("IWins", "ILoses");
				objective.setDisplaySlot(DisplaySlot.SIDEBAR);
				objective.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&c&l&nPrimeBall"));
				
				Score space1 = objective.getScore(" ");
				space1.setScore(14);
				
				Team mp = stats.registerNewTeam("kp");
				mp.addEntry(ChatColor.UNDERLINE + "");
				mp.setPrefix(ChatColor.translateAlternateColorCodes('&', "&6Time Left: " + timeLeft));
				objective.getScore(ChatColor.UNDERLINE + "").setScore(13);
				
				Score space2 = objective.getScore("  ");
				space2.setScore(12);
				
				Team mm = stats.registerNewTeam("km");
				mm.addEntry(ChatColor.BOLD + "");
				mm.setPrefix(ChatColor.translateAlternateColorCodes('&', "&cRed " + teamOneGoals ));
				mm.setSuffix(ChatColor.translateAlternateColorCodes('&', " &7- &bBlue " + teamTwoGoals));
				objective.getScore(ChatColor.BOLD + "").setScore(11);
				
				Score space3 = objective.getScore("   ");
				space3.setScore(10);
				
				Team mk = stats.registerNewTeam("kk");
				mk.addEntry(ChatColor.DARK_PURPLE + "");
				mk.setPrefix(ChatColor.translateAlternateColorCodes('&', "&6Last goal:"));
				objective.getScore(ChatColor.DARK_PURPLE + "").setScore(9);
				
				Team pg= stats.registerNewTeam("kg");
				pg.addEntry(ChatColor.DARK_RED + "");
				pg.setPrefix(ChatColor.translateAlternateColorCodes('&', "&6" + lastGoalScorer));
				
				player.setScoreboard(stats);
			} else {
				stats.getTeam("kp").setPrefix(ChatColor.translateAlternateColorCodes('&', "&6Time Left: " + timeLeft));
				stats.getTeam("km").setPrefix(ChatColor.translateAlternateColorCodes('&', "&cRed " + teamOneGoals));
				stats.getTeam("km").setSuffix(ChatColor.translateAlternateColorCodes('&',  " &7- &bBlue " + teamTwoGoals));
				if (lastGoalScorer != null) {
					String str;
					if (lastGoalScorer.length() > 14) str = lastGoalScorer.substring(0,14);
					else str = lastGoalScorer;
					stats.getTeam("kg").setPrefix(ChatColor.translateAlternateColorCodes('&', "&6" + str));
					stats.getObjective(DisplaySlot.SIDEBAR).getScore(ChatColor.DARK_RED + "").setScore(8);
				}
			}
		} 
	}
}
