package me.NavyDev.ArenaEvents;

import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAnimationEvent;

import me.navy12333.PrimeBall.PrimeBallMain;

public class PlayerKick implements Listener {

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerAnimation(PlayerAnimationEvent event) {
		Player player = event.getPlayer();
		List<Entity> ent = player.getNearbyEntities(1.0D, 1.0D, 1.0D);
		for (Entity entity : ent) {
			if (entity instanceof Item) {
				UUID id = entity.getUniqueId();	
				if (PrimeBallMain.plugin.BH.getBallIDs().contains(id)) {
					PrimeBallMain.plugin.BH.getItemBall().get(id).doKick(player);
					return;
				}
			}
		}
	}

}
