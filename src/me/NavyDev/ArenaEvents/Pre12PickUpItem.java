package me.NavyDev.ArenaEvents;

import java.util.UUID;

import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

import me.navy12333.PrimeBall.PrimeBallMain;

@SuppressWarnings("deprecation")
public class Pre12PickUpItem implements Listener {

	@EventHandler
	public void onArenaPlayerPickupItem(PlayerPickupItemEvent event) {
		Item item = event.getItem();
		UUID itemID = item.getUniqueId();
		if (PrimeBallMain.plugin.BH.getItemBalls() != null) {
			if (PrimeBallMain.plugin.BH.getBallIDs().contains(itemID)) {
				if (event.isCancelled()) {
					return;
				} else {
					event.setCancelled(true);
				}
			}
		}
	}

}
