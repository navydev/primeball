package me.NavyDev.ArenaEvents;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import me.NavyDev.Match.MatchHandler;
import me.navy12333.PrimeBall.PrimeBallMain;

public class PlayerFoodChange implements Listener {

	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		MatchHandler MH = PrimeBallMain.plugin.MH;
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			if (MH.getPlayers().contains(player)) {
				event.setFoodLevel(20);
				return;
			}
		}
		return;
	}
}
