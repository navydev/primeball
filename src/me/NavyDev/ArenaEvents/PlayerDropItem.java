package me.NavyDev.ArenaEvents;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

import me.NavyDev.Match.MatchHandler;
import me.navy12333.PrimeBall.PrimeBallMain;

public class PlayerDropItem implements Listener {

	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent event) {
		MatchHandler MH = PrimeBallMain.plugin.MH;
		Player player = event.getPlayer();
		ArrayList<Player> inGame = new ArrayList<Player>();
		inGame.addAll(MH.getPlayers());
		if (inGame.contains(player)) event.setCancelled(true);
	}
}
