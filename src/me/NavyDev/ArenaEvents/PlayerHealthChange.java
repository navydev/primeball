package me.NavyDev.ArenaEvents;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

import me.NavyDev.Match.MatchHandler;
import me.navy12333.PrimeBall.PrimeBallMain;

public class PlayerHealthChange implements Listener {

	@EventHandler
	public void onHealthChange(EntityDamageEvent event) {
		MatchHandler MH = PrimeBallMain.plugin.MH;
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			if (MH.getPlayers().contains(player)) {
				event.setCancelled(true);
				return;
			}
		}
		return;
	}
}
