package me.NavyDev.ArenaEvents;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;

import me.navy12333.PrimeBall.PrimeBallMain;

public class PlayerChangeHelmet implements Listener {
	
	@EventHandler
	public void drag(InventoryDragEvent event) {
		if (event.getWhoClicked() instanceof Player) {
			Player player = (Player) event.getWhoClicked();
			if (PrimeBallMain.plugin.MH.getPlayerMatches().containsKey(player)) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void click(InventoryClickEvent event) {
		if (event.getWhoClicked() instanceof Player) {
			Player player = (Player) event.getWhoClicked();
			if (PrimeBallMain.plugin.MH.getPlayerMatches().containsKey(player)) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void interact(InventoryInteractEvent event) {
		if (event.getWhoClicked() instanceof Player) {
			Player player = (Player) event.getWhoClicked();
			if (PrimeBallMain.plugin.MH.getPlayerMatches().containsKey(player)) {
				event.setCancelled(true);
			}
		}
	}
	

}
